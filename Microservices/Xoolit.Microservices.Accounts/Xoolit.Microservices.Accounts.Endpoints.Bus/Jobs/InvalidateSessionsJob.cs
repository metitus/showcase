﻿using Dywham.Middleware.Endpoints.Bus.Contracts.Jobs;
using NServiceBus;
using Quartz;
using Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Commands;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Jobs
{
    public class InvalidateSessionsJob : DywhamJob
    {
        public EndpointSettings EndpointSettings { get; set; }


        public override IJobDetail JobDetail => JobBuilder.Create<InvalidateSessionsJob>()
            .WithIdentity("InvalidateSessions")
            .Build();

        public override ITrigger Trigger => TriggerBuilder.Create()
            .ForJob(JobDetail)
            .WithCronSchedule(EndpointSettings.InvalidateSessionsJobTimeout)
            .WithIdentity("InvalidateSessionsJobTrigger")
            .StartNow()
            .Build();


        public override void Execute(IJobExecutionContext context, IMessageSession messageSession)
        {
            messageSession.SendLocal(new InvalidateSessionsCommand());
        }
    }
}