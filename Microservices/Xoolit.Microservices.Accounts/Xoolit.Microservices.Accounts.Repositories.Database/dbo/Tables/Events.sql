﻿CREATE TABLE [dbo].[Events] (
    [Id]                                  BIGINT           IDENTITY (1, 1) NOT NULL,
    [FullName]                            NVARCHAR (250)   NOT NULL,
    [Version]                             INT              NOT NULL,
    [Identity]                            NVARCHAR (250)   NOT NULL,
    [Name]                                NVARCHAR (250)   NOT NULL,
    [Payload]                             NVARCHAR (4000)  NOT NULL,
    [DateTime]                            DATETIME2 (7)    NOT NULL,
    [OriginatateInTheContextOfIdentifier] UNIQUEIDENTIFIER NULL,
    [OriginatateInTheContextOfName]       NVARCHAR (250)   NULL,
    [OriginatateInTheContextOfFullName]   NVARCHAR (250)   NULL,
    [OriginatateInTheContextOfPayload]    NVARCHAR (4000)  NULL,
    CONSTRAINT [PK_Events] PRIMARY KEY CLUSTERED ([Id] ASC)
);

