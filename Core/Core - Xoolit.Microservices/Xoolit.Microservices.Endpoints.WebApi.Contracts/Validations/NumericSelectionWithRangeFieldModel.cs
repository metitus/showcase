﻿using System.Collections.Generic;

namespace Xoolit.Microservices.Endpoints.WebApi.Contracts.Validations
{
    public class NumericSelectionWithRangeFieldModel : FieldModel
    {
        public IList<NumericSelectionWithRangeFieldOptionModel> Options { get; set; }
    }
}