﻿namespace Xoolit.Microservices.Accounts.Providers.PasswordHashing
{
    public class PasswordHashCreationResult
    {
        public string Hash { get; internal set; }

        public string Salt { get; internal set; }
    }
}