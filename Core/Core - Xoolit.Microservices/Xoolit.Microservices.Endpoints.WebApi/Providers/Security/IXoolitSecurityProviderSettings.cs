﻿namespace Xoolit.Microservices.Endpoints.WebApi.Providers.Security
{
    public interface IXoolitSecurityProviderSettings
    {
        string SecurityDataSource { get; set; }
    }
}
