﻿using System;
using Dywham.Middleware.Endpoints.Bus.Contracts.Messages.Events;
using Dywham.Middleware.Providers.EventStore.Contracts;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Events
{
    public class AccountActivatedEvent : MiddlewareEvent, IEventStoreIdentity
    {
        public string EmailActivationCode { get; set; }

        public int AccountStateId { get; set; }

        public Guid Id { get; set; }

        public int Version { get; set; }
    }
}