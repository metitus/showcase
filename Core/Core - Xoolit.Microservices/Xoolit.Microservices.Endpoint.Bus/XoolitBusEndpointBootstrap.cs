﻿using System.Data.SqlClient;
using Autofac;
using Dywham.Middleware.Endpoints.Bus;
using Dywham.Middleware.Endpoints.Bus.Contracts;
using NServiceBus;
using NServiceBus.Features;
using NServiceBus.Persistence;
using NServiceBus.Persistence.Sql;

namespace Xoolit.Microservices.Endpoint.Bus
{
    public abstract class XoolitBusEndpointBootstrap<T> : BusEndpointBootstrap<T> where T : SettingsForBusEndpoint
    {
        protected override void OnEndpointConfigurationStart(EndpointConfiguration configuration, ContainerBuilder builder)
        {
            configuration.EnableDurableMessages();
            configuration.EnableFeature<Sagas>();
            configuration.UseSerialization<JsonSerializer>();
            configuration.DisableFeature<TimeoutManager>();
            configuration.UseTransport<MsmqTransport>().Transactions(TransportTransactionMode.None);

            var persistence = configuration.UsePersistence<SqlPersistence>();

            persistence.SqlVariant(SqlVariant.MsSqlServer);
            persistence.ConnectionBuilder(() => new SqlConnection(PersistanceConnectionString));

            configuration.UsePersistence<SqlPersistence, StorageType.Subscriptions>();
            configuration.UsePersistence<SqlPersistence, StorageType.Sagas>();

            var subscriptions = persistence.SubscriptionSettings();

            subscriptions.DisableCache();
        }

        protected override void OnEndpointConfigurationCompleted(EndpointConfiguration configuration, IContainer container)
        {
            configuration.UseContainer<AutofacBuilder>(customizations =>
            {
                customizations.ExistingLifetimeScope(container);
            });
        }
    }
}