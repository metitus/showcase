﻿using System;
using Dywham.Middleware.Endpoints.Bus.Contracts.Messages.Events;
using Dywham.Middleware.Providers.EventStore.Contracts;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Events
{
    public class AccountCreatedEvent : MiddlewareEvent, IEventStoreIdentity
    {
        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public string EmailActivationCode { get; set; }

        public int? SmsActivationCode { get; set; }

        public string Culture { get; set; }

        public int AccountStateId { get; set; }

        public Guid Id { get; set; }

        public int Version { get; set; }

        public int Rating { get; set; }
    }
}