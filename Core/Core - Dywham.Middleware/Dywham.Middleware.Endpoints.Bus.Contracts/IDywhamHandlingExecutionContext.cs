﻿using System;
using System.Threading;
using Common.Logging;

namespace Dywham.Middleware.Endpoints.Bus.Contracts
{
    public interface IDywhamHandlingExecutionContext
    {
        IDywhamBus DywhamBus { get; }

        DateTime ExecutionStartDateTime { get; }

        CancellationToken CancellationToken { get; }

        ILog Log { get; }
    }
}
