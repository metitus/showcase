﻿using System.ComponentModel.DataAnnotations;
using Dywham.Middleware.Endpoints.WebApi.Commands.Contracts.Models;

namespace Xoolit.Microservices.Accounts.Endpoints.WebApi.Commands.Contracts
{
    public class ActivateAccountCommandModel : ApiCommandModel
    {
        [Required]
        public string EmailActivationCode { get; set; }

        public int? SmsActivationCode { get; set; }
    }
}