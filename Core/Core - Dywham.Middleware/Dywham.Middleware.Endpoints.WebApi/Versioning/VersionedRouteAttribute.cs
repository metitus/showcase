﻿using System;
using System.Collections.Generic;
using System.Web.Http.Routing;

namespace Dywham.Middleware.Endpoints.WebApi.Versioning
{
    /// <summary>
    /// Provides an attribute route that's restricted to a specific version of the api.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class VersionedRouteAttribute : RouteFactoryAttribute
    {
        public VersionedRouteAttribute(string template, int allowedVersion)
            : base(template)
        {
            AllowedVersion = allowedVersion;
        }


        public int AllowedVersion { get; }


        public override IDictionary<string, object> Constraints
        {
            get
            {
                var constraints = new HttpRouteValueDictionary
                {
                    {
                        "version", new VersionConstraint(AllowedVersion)
                    }
                };

                return constraints;
            }
        }
    }
}