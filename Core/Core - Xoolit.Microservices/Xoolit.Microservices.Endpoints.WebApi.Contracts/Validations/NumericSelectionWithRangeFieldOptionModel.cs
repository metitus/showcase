﻿namespace Xoolit.Microservices.Endpoints.WebApi.Contracts.Validations
{
    public class NumericSelectionWithRangeFieldOptionModel
    {
        public string Display { get; set; }

        public int Option { get; set; }

        public int? Min { get; set; }

        public int? Max { get; set; }
    }
}