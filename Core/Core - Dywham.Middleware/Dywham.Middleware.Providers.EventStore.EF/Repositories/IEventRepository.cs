﻿using System.Threading;
using System.Threading.Tasks;
using Dywham.Middleware.Providers.EventStore.EF.Repositories.Entities;
using Dywham.Middleware.Storage.Repositories.Contracts;

namespace Dywham.Middleware.Providers.EventStore.EF.Repositories
{
    public interface IEventRepository : IRepository<EventEntity>
    {
        void Add(EventEntity entity);

        Task AddAsync(EventEntity entity, CancellationToken token);
    }
}