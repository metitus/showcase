﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;

namespace Dywham.Middleware.Endpoints.WebApi.Contracts
{
    public class ApiMessageRequestEnvelope
    {
        public IEnumerable<KeyValuePair<string, string>> QueryNameValuePairs { get; set; }

        public HttpHeaders HttpHeaders { get; set; }

        public Type CommandType { get; set; }

        public string Body { get; set; }

        public string Route { get; set; }

        public string HttpMethod { get; set; }
    }
}