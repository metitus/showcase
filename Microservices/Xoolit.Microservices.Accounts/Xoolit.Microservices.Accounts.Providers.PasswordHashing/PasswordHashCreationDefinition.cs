namespace Xoolit.Microservices.Accounts.Providers.PasswordHashing
{
    public class PasswordHashCreationDefinition
    {
        public string Password { get; set; }

        public int SaltSize { get; set; }

        public int HashSize { get; set; }

        public byte[] Pepper { get; set; }

        public int Iterations { get; set; }
    }
}