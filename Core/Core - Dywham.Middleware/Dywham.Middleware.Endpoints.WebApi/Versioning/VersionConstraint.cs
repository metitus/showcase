﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;

namespace Dywham.Middleware.Endpoints.WebApi.Versioning
{
    /// <summary>
    /// A Constraint implementation that matches an HTTP query string against an expected version value.
    /// </summary>
    internal class VersionConstraint : IHttpRouteConstraint
    {
        public const string VersionQueryStringName = "api-version";
        private const int DefaultVersion = 1;


        public VersionConstraint(int allowedVersion)
        {
            AllowedVersion = allowedVersion;
        }


        public int AllowedVersion { get; }


        public bool Match(HttpRequestMessage request, IHttpRoute route, string parameterName, IDictionary<string, object> values, HttpRouteDirection routeDirection)
        {
            if (routeDirection != HttpRouteDirection.UriResolution) return true;

            var version = GetVersion(request) ?? DefaultVersion;
            var processRequest = version == AllowedVersion;

            if (processRequest) return true;

            HttpResponseMessage httpResponseMessage;
            var deprecatedVersionedRouteAttribute = request.GetActionDescriptor()
                .GetCustomAttributes<DeprecatedVersionedRouteAttribute>().FirstOrDefault();

            if (deprecatedVersionedRouteAttribute != null)
            {
                var httpStatusCode = deprecatedVersionedRouteAttribute.StillSupported ? HttpStatusCode.OK : HttpStatusCode.MovedPermanently;
                        
                //should we have an api reference page and point the redirect into thaty page?
                httpResponseMessage = request.CreateErrorResponse(httpStatusCode,
                    $"{deprecatedVersionedRouteAttribute.DeprecatedMessage} + {deprecatedVersionedRouteAttribute.NewVersion}");
            }
            else
            {
                //if the api is properly defined we should reach this point
                httpResponseMessage = request.CreateErrorResponse(HttpStatusCode.MovedPermanently, string.Empty);
            }

            throw new HttpResponseException(httpResponseMessage);
        }

        private static int? GetVersion(HttpRequestMessage request)
        {
            var versionAsString = request.GetQueryNameValuePairs()
                .Where(nv => nv.Key == VersionQueryStringName)
                .Select(nv => nv.Value).FirstOrDefault();

            if (versionAsString == null)
            {
                return null;
            }

            int version;

            if (int.TryParse(versionAsString, out version))
            {
                return version;
            }

            return null;
        }
    }
}