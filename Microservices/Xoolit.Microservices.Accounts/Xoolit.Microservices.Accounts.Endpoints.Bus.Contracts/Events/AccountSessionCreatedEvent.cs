﻿using System;
using Dywham.Middleware.Endpoints.Bus.Contracts.Messages.Events;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Events
{
    public class AccountSessionCreatedEvent : MiddlewareEvent
    {
        public Guid AccountId { get; set; }

        public Guid SessionId { get; set; }

        public string IpAddress { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }
    }
}