﻿namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts
{
    public enum SessionFinishedType
    {
        Timedout = 1,
        UserLoggedOut = 2
    }
}
