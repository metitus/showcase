﻿using Dywham.Middleware.Endpoints.WebApi;
using Xoolit.Microservices.Endpoints.WebApi.Providers.Security;

namespace Xoolit.Microservices.Endpoints.WebApi
{
    public class XoolitApiEndpoint : ApiEndpoint
    {
        public CurrentSessionInfo CurrentSessionInfo { get; internal set; }
    }
}