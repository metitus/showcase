﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Dywham.Middleware.Storage.Repositories.Contracts
{
    public interface IRepository
    { }

    public interface IRepository<T> : IRepository where T : class, new()
    {
        Task<TY> ResolveAsync<TY>(Func<IList<T>, TY> funcConvert, CancellationToken token);

        Task<TY> ResolveAsync<TY>(int limit, int startIndex, Func<IList<T>, long, TY> funcConvert,
            CancellationToken token) where TY : class;

        Task<TY> ResolveAsync<TY>(int limit, int startIndex, Func<IQueryable<T>, IQueryable<T>> func,
            Func<IList<T>, long, TY> funcConvert, CancellationToken token) where TY : class;

        Task<TY> ResolveAsync<TY>(int limit, int startIndex, OrderedSet orderedSet,
            Func<IList<T>, long, TY> funcConvert, CancellationToken token) where TY : class;

        Task<TY> ResolveAsync<TY>(int limit, int startIndex, OrderedSet orderedSet,
            Func<IQueryable<T>, IQueryable<T>> func,
            Func<IList<T>, long, TY> funcConvert, CancellationToken token) where TY : class;
    }
}