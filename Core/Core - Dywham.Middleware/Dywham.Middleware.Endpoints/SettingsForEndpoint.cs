﻿namespace Dywham.Middleware.Endpoints
{
    public abstract class SettingsForEndpoint
    {
        public string EndpointName { get; set; }

        public string SettingsFile { get; set; } = "EndpointSettings.json";
    }
}