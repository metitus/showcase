using System;

namespace Dywham.Middleware.Endpoints.WebApi.Contracts.Models
{
    public class ApiQueryForCollection
    {
        public int? Limit { get; set; }

        public int? StartIndex { get; set; }

        public string Filter { get; set; }
    }
}