﻿USE [Xoolit.Microservices.Accounts]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Accounts]
ON [dbo].[Accounts]([Mobile])
WHERE [Mobile] IS NOT NULL;
GO