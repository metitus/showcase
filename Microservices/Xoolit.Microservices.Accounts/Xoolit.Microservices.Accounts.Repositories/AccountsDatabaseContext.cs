﻿using System.Data.Entity;
using Dywham.Middleware.Storage.Repositories;
using Dywham.Middleware.Storage.Repositories.Contracts;
using Xoolit.Microservices.Accounts.Repositories.Entities;

namespace Xoolit.Microservices.Accounts.Repositories
{
    public class AccountsDatabaseContext : DatabaseContext, IDatabaseContextFactory<AccountsDatabaseContext>
    {
        public AccountsDatabaseContext()
        { }

        public AccountsDatabaseContext(string dataSource) : base(dataSource)
        { }


        public virtual DbSet<AccountEntity> Accounts { get; set; }

        public virtual DbSet<SessionEntity> Sessions { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountEntity>()
                .HasMany(e => e.Sessions)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);
        }

        public AccountsDatabaseContext CreateInstance(string dataSource)
        {
            return new AccountsDatabaseContext(dataSource);
        }
    }
}