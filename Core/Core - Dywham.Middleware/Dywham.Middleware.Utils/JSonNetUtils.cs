﻿using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Dywham.Middleware.Utils
{
    public static class JSonNetUtils
    {
        public static JToken ParseJson(string text)
        {
            using (var reader = new JsonTextReader(new StringReader(text)))
            {
                reader.DateParseHandling = DateParseHandling.None;

                return JToken.Load(reader);
            }
        }
    }
}