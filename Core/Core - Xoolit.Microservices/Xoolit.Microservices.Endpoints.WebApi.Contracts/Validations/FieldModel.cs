﻿namespace Xoolit.Microservices.Endpoints.WebApi.Contracts.Validations
{
    public class FieldModel
    {
        public string Name { get; set; }

        public bool? IsRequired { get; set; }
    }
}