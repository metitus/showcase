﻿using System.Data.Entity.Migrations;

namespace Xoolit.Microservices.Accounts.Repositories
{
    internal sealed class AccountsDatabaseContextConfiguration : DbMigrationsConfiguration<AccountsDatabaseContext>
    {
        public AccountsDatabaseContextConfiguration()
        {
            AutomaticMigrationsEnabled = false;
        }
    }
}
