﻿using System.Threading.Tasks;
using Dywham.Middleware.Endpoints.Bus.Contracts;
using NServiceBus;
using Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts;
using Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Commands;
using Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Events;
using Xoolit.Microservices.Accounts.Repositories;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Handlers
{
    public class ActivateAccountCommandHandler : IHandleMessages<ActivateAccountCommand>
    {
        public IDywhamHandlingExecution DywhamHandlingExecution { get; set; }

        public IAccountRepository AccountRepository { get; set; }


        public async Task Handle(ActivateAccountCommand message, IMessageHandlerContext context)
        {
            await DywhamHandlingExecution.HandlesAsync(context, message, async executionContext =>
            {
                var account = await AccountRepository.GetByActivationAsync(message.EmailActivationCode,
                    executionContext.CancellationToken);

                if (account == null)
                {
                    executionContext.Log.Info($"No account was found matching activation code: {message.EmailActivationCode}");

                    await executionContext.DywhamBus.PublishAsync(new AccountNotActivatedEvent
                    {
                        ActivationCode = message.EmailActivationCode,
                        Reason = AccountNotActivatedReason.EmailActivationCodeNotValid
                    }, executionContext.CancellationToken);

                    return;
                }

                if (account.AccountStateId == (int) AccountState.Active ||
                    account.AccountStateId == (int) AccountState.Deleted ||
                    account.AccountStateId == (int) AccountState.Suspended)
                {
                    executionContext.Log.Info($"Account matching activation code: {message.EmailActivationCode}, could not be activated because its status is: {account.AccountStateId}");

                    await executionContext.DywhamBus.PublishAsync(new AccountNotActivatedEvent
                    {
                        ActivationCode = message.EmailActivationCode,
                        Reason = AccountNotActivatedReason.EmailActivationCodeNotValid
                    }, executionContext.CancellationToken);

                    return;
                }

                if (account.AccountStateId == (int) AccountState.AwaitingEmailAndSmsVerification
                    && account.SmsActivationCode != message.SmsActivationCode)
                {
                    executionContext.Log.Info($"Account matching activation code: {message.EmailActivationCode}, could not be activated because the sms activation code does not match");

                    await executionContext.DywhamBus.PublishAsync(new AccountNotActivatedEvent
                    {
                        ActivationCode = message.EmailActivationCode,
                        Reason = AccountNotActivatedReason.SmsActivationCodeNotValid
                    }, executionContext.CancellationToken);

                    return;
                }

                account.AccountStateId = (int)AccountState.Active;
                account.DateTimeLastChanged = executionContext.ExecutionStartDateTime;
                account.Version = account.Version++;

                await AccountRepository.UpdateAsync(account, executionContext.CancellationToken);

                await executionContext.DywhamBus.PublishAsync(new AccountActivatedEvent
                {
                    EmailActivationCode = message.EmailActivationCode,
                    AccountStateId = account.AccountStateId,
                    Id = account.ExternalId,
                    Version = account.Version
                }, executionContext.CancellationToken);
            });
        }
    }
}