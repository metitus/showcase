﻿using System;

namespace Dywham.Middleware.Providers.EventStore.Contracts
{
    public interface IEventStoreIdentity
    {
        Guid Id { get; set; }

        int Version { get; set; }
    }
}