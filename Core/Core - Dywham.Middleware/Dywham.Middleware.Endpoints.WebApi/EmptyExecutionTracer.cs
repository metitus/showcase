﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Dywham.Middleware.Endpoints.WebApi
{
    public class EmptyExecutionTracer : IExecutionTracer
    {
        public Task BeforeHandlingAsync(string route, string method, object payload, Dictionary<string, object> requestValues, CancellationToken token)
        {
            return Task.CompletedTask;
        }

        public Task AfterHandlingAsync(string route, string method, object payload, Dictionary<string, object> requestValues, CancellationToken token)
        {
            return Task.CompletedTask;
        }

        public Task OnExceptionAsync(string route, string method, object payload, Dictionary<string, object> requestValues, Exception ex,
            CancellationToken token)
        {
            return Task.CompletedTask;
        }
    }
}