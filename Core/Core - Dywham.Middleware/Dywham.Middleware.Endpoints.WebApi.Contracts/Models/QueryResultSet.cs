﻿using System.Collections.Generic;

namespace Dywham.Middleware.Endpoints.WebApi.Contracts.Models
{
    public class QueryResultSet<T>
    {
        public IList<T> Set { get; set; }

        public long TotalCount { get; set; }
    }
}
