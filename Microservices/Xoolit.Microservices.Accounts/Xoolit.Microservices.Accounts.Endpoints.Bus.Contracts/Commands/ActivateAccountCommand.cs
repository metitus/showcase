﻿using Dywham.Middleware.Endpoints.Bus.Contracts.Messages.Commands;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Commands
{
    public class ActivateAccountCommand : MiddlewareCommand
    {
        public string EmailActivationCode { get; set; }

        public int? SmsActivationCode { get; set; }
    }
}