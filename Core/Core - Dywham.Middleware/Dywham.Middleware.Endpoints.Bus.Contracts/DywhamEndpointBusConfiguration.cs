﻿namespace Dywham.Middleware.Endpoints.Bus.Contracts
{
    public class DywhamEndpointBusConfiguration : DywhamEndpointConfiguration
    {
        public string AuditQueue { get; set; }

        public string ErrorQueue { get; set; }

        public bool EnableInstallers { get; set; }

        public bool EnableJobScheduler { get; set; }
    }
}