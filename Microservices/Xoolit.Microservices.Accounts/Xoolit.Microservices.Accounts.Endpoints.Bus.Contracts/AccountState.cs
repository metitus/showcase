﻿namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts
{
    public enum AccountState
    {
        AwaitingEmailActivation = 1,
        AwaitingEmailAndSmsVerification = 2,
        Active = 3,
        Suspended = 4,
        Deleted = 5
    }
}
