﻿using System.Data.Entity.Migrations;

namespace Xoolit.Microservices.Endpoints.WebApi.Providers.Security.Repositories
{
    internal sealed class SecurityDatabaseContextConfiguration : DbMigrationsConfiguration<SecurityDatabaseContext>
    {
        public SecurityDatabaseContextConfiguration()
        {
            AutomaticMigrationsEnabled = false;
        }
    }
}
