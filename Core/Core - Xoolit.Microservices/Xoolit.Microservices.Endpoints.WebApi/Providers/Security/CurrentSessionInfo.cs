using System;

namespace Xoolit.Microservices.Endpoints.WebApi.Providers.Security
{
    public class CurrentSessionInfo
    {
        public Guid AccountId { get; internal set; }

        public Guid SessionId { get; internal set; }
    }
}