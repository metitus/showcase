﻿namespace Xoolit.Microservices.Endpoints.WebApi.Contracts.Validations
{
    public class NumericSelectionFieldOptionModel
    {
        public string Display { get; set; }

        public int Option { get; set; }
    }
}