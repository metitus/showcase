﻿using System.Collections.Generic;

namespace Dywham.Middleware.Endpoints.WebApi.Contracts.Models
{
    public class ValidationResult
    {
        public Dictionary<string, object> ValidationIssues { get; set; } = new Dictionary<string, object>();

        public string Message { get; set; }
    }
}