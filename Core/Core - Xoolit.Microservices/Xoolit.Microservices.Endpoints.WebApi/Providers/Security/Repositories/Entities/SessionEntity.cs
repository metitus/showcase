﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Xoolit.Microservices.Endpoints.WebApi.Providers.Security.Repositories.Entities
{
	[Table("Sessions")]
	public class SessionEntity
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public Guid ExternalId { get; set; }

		public Guid UserId { get; set; }

		[Column(TypeName = "datetime2")]
		public DateTime DateTimeStarted { get; set; }

		[Column(TypeName = "datetime2")]
		public DateTime? DateTimeFinished { get; set; }
		
		public string IpAddress { get; set; }
	}
}