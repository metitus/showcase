﻿using System;

namespace Xoolit.Microservices.Endpoints.WebApi.Contracts.Commands
{
    public interface IRequiresIdentity
    {
        Guid UserId { get; set; }
    }
}