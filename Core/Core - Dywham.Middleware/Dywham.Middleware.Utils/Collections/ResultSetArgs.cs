﻿namespace Dywham.Middleware.Utils.Collections
{
    public abstract class ResultSetArgs
    {
        public int Limit { get; set; }

        public int StartIndex { get; set; }

        public string Filter { get; set; }
    }
}
