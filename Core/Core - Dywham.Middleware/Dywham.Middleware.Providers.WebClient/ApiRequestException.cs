﻿using System.Net;
using System.Net.Http;

namespace Dywham.Middleware.Providers.WebClient
{
    public class ApiRequestException : HttpRequestException
    {
        public HttpStatusCode StatusCode { get; }

        public ApiRequestException(HttpStatusCode statusCode, string content) : base(content)
        {
            StatusCode = statusCode;
        }
    }
}
