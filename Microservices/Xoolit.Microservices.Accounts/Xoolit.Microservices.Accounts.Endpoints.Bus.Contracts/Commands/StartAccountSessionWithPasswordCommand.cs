﻿using Dywham.Middleware.Endpoints.Bus.Contracts.Messages.Commands;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Commands
{
    public class StartAccountSessionWithPasswordCommand : MiddlewareCommand
    {
        public string Username { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string IpAddress { get; set; }
    }
}