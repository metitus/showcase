namespace Dywham.Middleware.Endpoints.WebApi.Contracts.Models
{
    public class ApiQueryForTableCollection : ApiQueryForCollection
    {
        public bool Asc { get; set; } = true;

        public string ColumnName { get; set; }
    }
}