﻿namespace Dywham.Middleware.Endpoints.Bus.Contracts
{
    public class SettingsForBusEndpoint : SettingsForEndpoint
    {
        public string ErrorQueue { get; set; }

        public string PersistanceConnectionString { get; set; }
    }
}