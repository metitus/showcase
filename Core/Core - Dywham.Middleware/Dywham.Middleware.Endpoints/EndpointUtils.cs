﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Autofac;
using AutoMapper;
using Newtonsoft.Json;

namespace Dywham.Middleware.Endpoints
{
    public static class EndpointUtils
    {
        public static void InitializeAutoMapper(IList<Profile> profiles)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMissingTypeMaps = true;
                cfg.AllowNullDestinationValues = true;
                cfg.AllowNullCollections = true;

                foreach (var profile in profiles)
                {
                    cfg.AddProfile(profile);
                }

            });
        }

        public static T LoadComponentsConfigurationFiles<T>(Type settingsType, string path, EndpointBootstrap endpoint,
            ContainerBuilder builder) where T : SettingsForEndpoint
        {
            try
            {
                var settings = (SettingsForEndpoint)Activator.CreateInstance(settingsType);

                if (string.IsNullOrWhiteSpace(settings.SettingsFile))
                {
                    throw new InvalidOperationException($"SettingsFile is null for type {settingsType.FullName}");
                }

                var configFilePath = Path.Combine(path, settings.SettingsFile);

                if (!File.Exists(configFilePath)) return null;

                var configuration = JsonConvert.DeserializeObject(File.ReadAllText(configFilePath), settingsType);
                var built = builder.Register(x => configuration).As(settingsType);

                settingsType.GetInterfaces().ToList().ForEach(i => built.As(i));

                var settingsToSetInTheBootstrap = endpoint.GetType().GetProperties()
                    .FirstOrDefault(p => typeof(SettingsForEndpoint).IsAssignableFrom(p.PropertyType));

                if (settingsToSetInTheBootstrap != null)
                {
                    settingsToSetInTheBootstrap.SetValue(endpoint, configuration);
                }

                return (T)configuration;
            }
            catch (InvalidOperationException exception)
            {
                throw new InvalidOperationException($"Could not register custom configuration of type {settingsType.FullName}, exception -> {exception.Message}");
            }
        }

        public static T LoadComponentsConfigurationFilesAndSet<T>(Type settingsType, string path, EndpointBootstrap endpoint,
            ContainerBuilder builder) where T : SettingsForEndpoint
        {
            var settings = LoadComponentsConfigurationFiles<T>(typeof(T),
                AppDomain.CurrentDomain.BaseDirectory, endpoint, builder);

            SetSettings(endpoint, settings);

            return settings;
        }

        public static void SetSettings(EndpointBootstrap endpointBootstrap, SettingsForEndpoint settings)
        {
            if (!string.IsNullOrWhiteSpace(settings.EndpointName))
            {
                endpointBootstrap.EndpointName = settings.EndpointName;
            }
        }
    }
}