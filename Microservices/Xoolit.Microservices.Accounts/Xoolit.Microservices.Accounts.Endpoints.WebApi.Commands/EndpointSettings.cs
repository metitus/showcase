﻿using Dywham.Middleware.Endpoints.WebApi.Commands.Contracts;
using Dywham.Middleware.Storage.Repositories.Contracts;

namespace Xoolit.Microservices.Accounts.Endpoints.WebApi.Commands
{
    public class EndpointSettings : SettingsForApiCommandsEndpoint, IRepositoriesSettings
    {
        public string LoginPageUrl { get; set; }

        public string DataSource { get; set; }
    }
}