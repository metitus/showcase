﻿CREATE TABLE [dbo].[Sessions] (
    [Id]                    BIGINT           IDENTITY (1, 1) NOT NULL,
    [AccountId]             BIGINT           NOT NULL,
    [ExternalId]            UNIQUEIDENTIFIER NOT NULL,
    [DateTimeStarted]       DATETIME2 (7)    NOT NULL,
    [DateTimeFinished]      DATETIME2 (7)    NULL,
    [LastDateTimePing]      DATETIME2 (7)    NOT NULL,
    [IpAddress]             NVARCHAR (50)    NOT NULL,
    [SessionFinishedTypeId] INT              NULL,
    [UserId]                UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_Sessions] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Sessions_Accounts] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Accounts] ([Id]),
    CONSTRAINT [FK_Sessions_SessionFinishedType] FOREIGN KEY ([SessionFinishedTypeId]) REFERENCES [dbo].[SessionFinishedTypes] ([Id])
);

