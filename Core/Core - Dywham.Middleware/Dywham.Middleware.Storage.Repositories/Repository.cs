using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading;
using System.Threading.Tasks;
using Dywham.Middleware.Storage.Repositories.Contracts;
using Z.EntityFramework.Plus;

namespace Dywham.Middleware.Storage.Repositories
{
    public abstract class Repository<T, TZ> : IRepository<T> where T : class, new() where TZ : DbContext
    {
        protected Repository(IRepositoriesSettings setting, IDatabaseContextFactory<TZ> databaseContextFactory)
        {
            DbContext = databaseContextFactory.CreateInstance(setting.DataSource);
        }


        protected TZ DbContext { get; set; }


        public void UseDbContext(TZ dbContext)
        {
            DbContext = dbContext;
        }
        
        public async Task<TY> ResolveAsync<TY>(Func<IList<T>, TY> funcConvert, CancellationToken token)
        {
            var query = DbContext.Set<T>().AsNoTracking().AsQueryable();

            return funcConvert(await query.ToListAsync(token));
        }

        public async Task<TY> ResolveAsync<TY>(int limit, int startIndex, Func<IList<T>, long, TY> funcConvert,
            CancellationToken token) where TY : class
        {
            var query = DbContext.Set<T>().AsNoTracking().AsQueryable();

            if (limit > 0)
            {
                query = query.Skip(() => startIndex).Take(() => limit);
            }

            return funcConvert(await query.ToListAsync(token), query.DeferredCount().FutureValue());
        }

        public async Task<TY> ResolveAsync<TY>(int limit, int startIndex, Func<IQueryable<T>, IQueryable<T>> func,
            Func<IList<T>, long, TY> funcConvert, CancellationToken token) where TY : class
        {
            var query = func.Invoke(DbContext.Set<T>().AsNoTracking().AsQueryable()).AsQueryable();

            if (limit > 0)
            {
                query = query.Skip(() => startIndex).Take(() => limit);
            }

            return funcConvert(await query.ToListAsync(token), query.DeferredCount().FutureValue());
        }

        public async Task<TY> ResolveAsync<TY>(int limit, int startIndex, OrderedSet orderedSet,
            Func<IList<T>, long, TY> funcConvert, CancellationToken token) where TY : class
        {
            var query = DbContext.Set<T>().AsNoTracking().AsQueryable();

            query = query.OrderBy(orderedSet.ColumnName, orderedSet.Asc);

            if (limit > 0)
            {
                query = query.Skip(() => startIndex).Take(() => limit);
            }

            return funcConvert(await query.ToListAsync(token), query.DeferredCount().FutureValue());
        }

        public async Task<TY> ResolveAsync<TY>(int limit, int startIndex, OrderedSet orderedSet,
            Func<IQueryable<T>, IQueryable<T>> func,
            Func<IList<T>, long, TY> funcConvert, CancellationToken token) where TY : class
        {
            var query = func.Invoke(DbContext.Set<T>().AsNoTracking().AsQueryable()).AsQueryable();

            query = query.OrderBy(orderedSet.ColumnName + (orderedSet.Asc? "": " descending"));

            if (limit > 0)
            {
                query = query.Skip(() => startIndex).Take(() => limit);
            }

            return funcConvert(await query.ToListAsync(token), query.DeferredCount().FutureValue());
        }
    }
}