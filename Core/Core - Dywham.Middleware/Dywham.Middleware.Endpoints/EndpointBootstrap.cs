﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using AutoMapper;
using Dywham.Middleware.Utils;

namespace Dywham.Middleware.Endpoints
{
    public abstract class EndpointBootstrap
    {
        protected ContainerBuilder ContainerBuilder;


        public bool EnableAutoMapperProfilesScanning { get; set; } = true;

        public string EndpointName { get; set; } = "EndpointName";

        protected string DefaultExecutionPath { get; set; } = string.Empty;


        protected void LoadMappings()
        {
            var mappings = new List<Profile>();
            var defaultProfile = new MappingProfile();

            ConfigureMappings(defaultProfile);

            mappings.Add(defaultProfile);

            if (EnableAutoMapperProfilesScanning)
            {
                mappings.AddRange(AssemblyUtils.GetTypesMatching(DefaultExecutionPath, x =>
                        typeof(MappingProfile).IsAssignableFrom(x) && x.IsClass && !x.IsAbstract)
                    .Select(Activator.CreateInstance)
                    .Cast<Profile>()
                    .ToList());
            }

            EndpointUtils.InitializeAutoMapper(mappings);
        }

        protected virtual void ConfigureMappings(MappingProfile profile)
        { }

        protected virtual void SetSettings()
        { }
    }
}