﻿namespace Xoolit.Microservices.Endpoints.WebApi.Contracts.Security
{
    public enum ResourceAccessMode
    {
        Anonymous = 1,
        ActiveSession = 2
    }
}