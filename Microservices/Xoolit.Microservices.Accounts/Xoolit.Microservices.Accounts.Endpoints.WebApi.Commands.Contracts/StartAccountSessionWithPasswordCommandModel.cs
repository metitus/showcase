﻿using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using Dywham.Middleware.Endpoints.WebApi.Contracts.Models;
using ValidationResult = Dywham.Middleware.Endpoints.WebApi.Contracts.Models.ValidationResult;
using Dywham.Middleware.Endpoints.WebApi.Commands.Contracts.Models;

namespace Xoolit.Microservices.Accounts.Endpoints.WebApi.Commands.Contracts
{
    public class StartAccountSessionWithPasswordCommandModel : ApiCommandModel, IModelValidator<StartAccountSessionWithPasswordCommandModel>
    {
        public string Username { get; set; }

        public string Email { get; set; }

        [Required]
        public string Password { get; set; }


        public ValidationResult Validate(StartAccountSessionWithPasswordCommandModel model, HttpRequestMessage httpRequestMessage)
        {
            var validationResult = new ValidationResult();

            if (string.IsNullOrEmpty(model.Username) && string.IsNullOrEmpty(model.Password))
            {
                validationResult.ValidationIssues.Add("Username/Email", "Either an email or an account must be provided");
            }

            return validationResult;
        }
    }
}
