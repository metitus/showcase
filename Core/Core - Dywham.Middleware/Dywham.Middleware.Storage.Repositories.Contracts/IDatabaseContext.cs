﻿using System.Threading;
using System.Threading.Tasks;

namespace Dywham.Middleware.Storage.Repositories.Contracts
{
    public interface IDatabaseContext
    {
        void Init();

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}