﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Dywham.Middleware.Endpoints.WebApi
{
    public interface IExecutionTracer
    {
        Task BeforeHandlingAsync(string route, string method, object payload, Dictionary<string, object> requestValues,
            CancellationToken token);

        Task AfterHandlingAsync(string route, string method, object payload, Dictionary<string, object> requestValues,
            CancellationToken token);

        Task OnExceptionAsync(string route, string method, object payload, Dictionary<string, object> requestValues,
            Exception ex, CancellationToken token);
    }
}