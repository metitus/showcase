﻿using Dywham.Middleware.Endpoints.Bus.Contracts;
using Dywham.Middleware.Storage.Repositories.Contracts;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus
{
    public class EndpointSettings : SettingsForBusEndpoint, IRepositoriesSettings
    {
        public string DataSource { get; set; }

        public string InvalidateSessionsJobTimeout { get; set; }

        public int InvalidateSessionsTimeout { get; set; }

        public int InitialAccountRating { get; set; }
    }
}