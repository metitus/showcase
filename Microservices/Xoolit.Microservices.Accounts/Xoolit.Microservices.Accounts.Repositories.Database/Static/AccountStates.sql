﻿USE [Xoolit.Microservices.Accounts]
GO
INSERT [dbo].[AccountStates] ([Id], [Name]) VALUES (1, N'AwaitingEmailActivation')
GO
INSERT [dbo].[AccountStates] ([Id], [Name]) VALUES (2, N'AwaitingEmailAndSmsVerification')
GO
INSERT [dbo].[AccountStates] ([Id], [Name]) VALUES (3, N'Active')
GO
INSERT [dbo].[AccountStates] ([Id], [Name]) VALUES (4, N'Suspended')
GO
INSERT [dbo].[AccountStates] ([Id], [Name]) VALUES (5, N'Deleted')
GO