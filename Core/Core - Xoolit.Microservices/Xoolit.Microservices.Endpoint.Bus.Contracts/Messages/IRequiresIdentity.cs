﻿using System;

namespace Xoolit.Microservices.Endpoint.Bus.Contracts.Messages
{
    public interface IRequiresIdentity
    {
        Guid UserId { get; set; }
    }
}
