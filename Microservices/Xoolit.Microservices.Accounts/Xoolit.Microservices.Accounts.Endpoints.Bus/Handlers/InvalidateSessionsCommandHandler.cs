﻿using System.Linq;
using System.Threading.Tasks;
using Dywham.Middleware.Endpoints.Bus.Contracts;
using NServiceBus;
using Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts;
using Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Commands;
using Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Events;
using Xoolit.Microservices.Accounts.Repositories;
using Xoolit.Microservices.Accounts.Repositories.Entities;
using Dywham.Middleware.Endpoints.Bus.Handling;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Handlers
{
    public class InvalidateSessionsCommandHandler : IHandleMessages<CreateAccountCommand>
    {
        public EndpointSettings EndpointSettings { get; set; }

        public ISessionRepository SessionRepository { get; set; }

        public DywhamHandlingExecution DywhamHandlingExecution { get; set; }


        public async Task Handle(CreateAccountCommand message, IMessageHandlerContext context)
        {
            await DywhamHandlingExecution.HandlesAsync(context, message, async executionContext =>
            {
                await SessionRepository
                    .UpdateBulkAsync(x => (x.LastDateTimePing - x.DateTimeFinished)?.Seconds > EndpointSettings.InvalidateSessionsTimeout,
                        new SessionEntity
                        {
                            SessionFinishedTypeId = (int)SessionFinishedType.Timedout,
                            DateTimeFinished = executionContext.ExecutionStartDateTime
                        }, executionContext.CancellationToken);

                var invalidatedSession = await SessionRepository.GetByAsync(new SessionEntityQueryParms
                    {
                        DateTimeFinished = executionContext.ExecutionStartDateTime
                    }, executionContext.CancellationToken);

                await executionContext.DywhamBus.PublishAsync(new InvalidatedSessionsEvent
                    {
                       Sessions = invalidatedSession.Select(x => x.ExternalId).ToList()
                    }, executionContext.CancellationToken);
            });
        }
    }
}
