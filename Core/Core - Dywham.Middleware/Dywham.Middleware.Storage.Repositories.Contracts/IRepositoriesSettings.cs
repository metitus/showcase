﻿namespace Dywham.Middleware.Storage.Repositories.Contracts
{
    public interface IRepositoriesSettings
    {
        string DataSource { get; set; }
    }
}