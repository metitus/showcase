using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Dywham.Middleware.Endpoints.Bus.Contracts;
using Dywham.Middleware.Endpoints.Bus.Contracts.Jobs;
using NServiceBus;
using Quartz;
using Quartz.Impl;

namespace Dywham.Middleware.Endpoints.Bus.Jobs
{
    internal class BusEndpointBootstrapStartup : IWantToRunWhenEndpointStartsAndStops
    {
        protected static IScheduler Scheduler;


        public DywhamEndpointBusConfiguration DywhamEndpointBusConfiguration { get; set; }

        public IComponentContext ComponentContext { get; set; }


        public Task Start(IMessageSession session)
        {
            if (!DywhamEndpointBusConfiguration.EnableJobScheduler) return Task.CompletedTask;

            var jobs = ComponentContext.Resolve<IEnumerable<DywhamJob>>().ToList();

            if (!jobs.Any()) return Task.CompletedTask;

            Scheduler = new StdSchedulerFactory().GetScheduler();

            Scheduler.Context.Put("MessageSession", session);

            foreach (var job in jobs)
            {
                Scheduler.ScheduleJob(job.JobDetail, job.Trigger);
            }

            Scheduler.Start();

            return Task.CompletedTask;
        }

        public Task Stop(IMessageSession session)
        {
            if (!Scheduler.IsStarted) return Task.CompletedTask;

            Scheduler.Shutdown(true);

            return Task.CompletedTask; ;
        }
    }
}