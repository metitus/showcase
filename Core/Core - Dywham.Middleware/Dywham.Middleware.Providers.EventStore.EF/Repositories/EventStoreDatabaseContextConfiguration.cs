﻿using System.Data.Entity.Migrations;

namespace Dywham.Middleware.Providers.EventStore.EF.Repositories
{
    internal sealed class EventStoreDatabaseContextConfiguration : DbMigrationsConfiguration<EventStoreDatabaseContextContext>
    {
        public EventStoreDatabaseContextConfiguration()
        {
            AutomaticMigrationsEnabled = false;
        }
    }
}
