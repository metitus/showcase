using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Xoolit.Microservices.Storage.Repositories
{
    public class XoolitEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public Guid ExternalId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DateTimeCreated { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DateTimeLastChanged { get; set; }
    }
}