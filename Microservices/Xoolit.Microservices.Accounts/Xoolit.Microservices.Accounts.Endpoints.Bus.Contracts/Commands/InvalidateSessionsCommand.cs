﻿using Dywham.Middleware.Endpoints.Bus.Contracts.Messages.Commands;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Commands
{
    public class InvalidateSessionsCommand : MiddlewareCommand
    { }
}