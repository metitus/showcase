﻿using System;
using System.Security.Cryptography;

namespace Xoolit.Microservices.Accounts.Providers.PasswordHashing
{
    public sealed class PasswordHashingProvider : IPasswordHashingProvider
    {
        private const int SaltSize = 16;
        private const int HashSize = 20;
        private const int DefaultNoIterarions = 1000;
        private const string HashVersion = "DYWHAM$VERSION1";
        

        public string CreateHash(string password)
        {
            return CreateHash(password, DefaultNoIterarions);
        }

        public string CreateHash(string password, int iterations)
        {
            byte[] salt;

            using (var cryptoProvider = new RNGCryptoServiceProvider())
            {
                cryptoProvider.GetBytes(salt = new byte[SaltSize]);
            }

            byte[] hash;

            using (var pbkdf2 = new Rfc2898DeriveBytes(password, salt, iterations))
            {
                hash = pbkdf2.GetBytes(HashSize);
            }

            var hashBytes = new byte[SaltSize + HashSize];

            Array.Copy(salt, 0, hashBytes, 0, SaltSize);
            Array.Copy(hash, 0, hashBytes, SaltSize, HashSize);

            return $"{HashVersion}{iterations}_{Convert.ToBase64String(hashBytes)}";
        }

        public bool Verify(string password, string hashedPassword)
        {
            if (!IsHashSupported(hashedPassword))
            {
                throw new NotSupportedException("The hashtype is not supported");
            }

            var splittedHashString = hashedPassword.Replace(HashVersion, "").Split('_');
            var iterations = int.Parse(splittedHashString[0]);
            var base64Hash = splittedHashString[1];
            var hashBytes = Convert.FromBase64String(base64Hash);
            var salt = new byte[SaltSize];
            Array.Copy(hashBytes, 0, salt, 0, SaltSize);
            byte[] hash;

            using (var pbkdf2 = new Rfc2898DeriveBytes(password, salt, iterations))
            {
                hash = pbkdf2.GetBytes(HashSize);
            }

            for (var i = 0; i < HashSize; i++)
            {
                if (hashBytes[i + SaltSize] != hash[i])
                {
                    return false;
                }
            }

            return true;
        }

        private static bool IsHashSupported(string hashVersion)
        {
            return hashVersion.Contains(HashVersion);
        }
    }
}