﻿using Dywham.Middleware.Endpoints.Bus.Contracts.Messages.Commands;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Commands
{
    public class CreateAccountCommand : MiddlewareCommand
    {
        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public string Culture { get; set; }
    }
}