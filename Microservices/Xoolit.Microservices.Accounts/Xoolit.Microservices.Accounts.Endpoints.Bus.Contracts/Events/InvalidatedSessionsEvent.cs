﻿using System;
using System.Collections.Generic;
using Dywham.Middleware.Endpoints.Bus.Contracts.Messages.Events;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Events
{
    public class InvalidatedSessionsEvent : MiddlewareEvent
    {
        public List<Guid> Sessions { get; set; }
    }
}