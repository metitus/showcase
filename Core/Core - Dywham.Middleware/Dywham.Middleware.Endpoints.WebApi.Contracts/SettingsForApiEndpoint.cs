﻿namespace Dywham.Middleware.Endpoints.WebApi.Contracts
{
    public class SettingsForApiEndpoint : SettingsForEndpoint
    {
        public EndpointExecutionMode? ExecutionMode { get; set; }

        public string EndpointVersion { get; set; }

        public string EndpointDescription { get; set; }
    }
}
