﻿using System.Threading;
using System.Threading.Tasks;

namespace Dywham.Middleware.Providers.EventStore.Contracts
{
    public interface IEventStoreProvider
    {
        Task StoreAsync(Event @event, CancellationToken token);
    }
}