﻿using Autofac;
using Dywham.Middleware.Storage.Repositories.Contracts;
using Dywham.Middleware.Utils;

namespace Dywham.Middleware.Storage.Repositories
{
    public class RepositoriesAutofacModules : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(AssemblyUtils.Assemblies(null))
                .Where(t => typeof(IRepository).IsAssignableFrom(t))
                .AsImplementedInterfaces()
                .InstancePerDependency()
                .PropertiesAutowired();

            builder.RegisterAssemblyTypes(AssemblyUtils.Assemblies(null))
                .Where(t => typeof(IDatabaseContextFactory).IsAssignableFrom(t))
                .AsImplementedInterfaces()
                .SingleInstance()
                .PropertiesAutowired();
        }
    }
}
