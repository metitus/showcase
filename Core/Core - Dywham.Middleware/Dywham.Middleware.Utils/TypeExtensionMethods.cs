﻿using System;

namespace Dywham.Middleware.Utils
{
    public static class TypeExtensionMethods
    {
        public static bool IsConcreteDerivedTypeOf(this Type type, Type baseType)
        {
            return baseType.IsAssignableFrom(type) && type.IsClass && !type.IsAbstract;
        }

        public static bool IsConcreteDerivedTypeOf<TBase>(this Type type)
        {
            return IsConcreteDerivedTypeOf(type, typeof(TBase));
        }
    }
}
