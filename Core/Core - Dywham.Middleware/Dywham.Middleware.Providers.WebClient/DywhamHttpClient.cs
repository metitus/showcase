﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Dywham.Middleware.Providers.WebClient
{
    public class DywhamHttpClient : IDisposable, IDywhamHttpClient
    {
        public DywhamHttpClient() : this(false)
        { }

        public DywhamHttpClient(bool useDefaultCredentials) : this (useDefaultCredentials, null)
        { }

        public DywhamHttpClient(HttpMessageHandler httpHandler) : this(false, httpHandler)
        { }

        public DywhamHttpClient(bool useDefaultCredentials, HttpMessageHandler httpHandler)
        {
            if (httpHandler == null)
            {
                HttpClient = useDefaultCredentials
                    ? new HttpClient(new HttpClientHandler { UseDefaultCredentials = true })
                    : new HttpClient(new HttpClientHandler());
            }
            else
            {
                HttpClient = new HttpClient(httpHandler);
            }

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3;

            HttpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            HttpClient.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
        }


        public HttpClient HttpClient { get; set; }


        public DywhamHttpClient AddDefaultRequestHeaders(Dictionary<string, object> defaultRequestHeaders)
        {
            if (defaultRequestHeaders == null || !defaultRequestHeaders.Any()) return this;

            foreach (var key in defaultRequestHeaders.Keys)
            {
                HttpClient.DefaultRequestHeaders.TryAddWithoutValidation(key, defaultRequestHeaders[key].ToString());
            }

            return this;
        }

        public void DoDelete(SubmitDywhamHttpRequestParams httpRequestParams = null)
        {
            DoRequestAsync(HttpMethod.Delete, httpRequestParams, CancellationToken.None).RunSynchronously();
        }

        public async Task DoDeleteAsync(string url, CancellationToken ct)
        {
            await DoDeleteAsync(new SubmitDywhamHttpRequestParams { Url = url }, ct);
        }

        public async Task DoDeleteAsync(string url, object payload, CancellationToken ct)
        {
            await DoDeleteAsync(new SubmitDywhamHttpRequestParams { Url = url, Data = payload }, ct);
        }

        public async Task DoDeleteAsync(SubmitDywhamHttpRequestParams httpRequestParams, CancellationToken ct)
        {
            await DoRequestAsync(HttpMethod.Delete, httpRequestParams, ct);
        }

        public void DoPost(SubmitDywhamHttpRequestParams httpRequestParams = null)
        {
            DoRequestAsync(HttpMethod.Post, httpRequestParams, CancellationToken.None).RunSynchronously();
        }

        public async Task DoPostAsync(string url, CancellationToken ct)
        {
            await DoPostAsync(new SubmitDywhamHttpRequestParams { Url = url }, ct);
        }

        public async Task DoPostAsync(string url, object payload, CancellationToken ct)
        {
            await DoPostAsync(new SubmitDywhamHttpRequestParams { Url = url, Data = payload }, ct);
        }

        public async Task DoPostAsync(SubmitDywhamHttpRequestParams httpRequestParams, CancellationToken ct)
        {
            await DoRequestAsync(HttpMethod.Post, httpRequestParams, ct);
        }

        public void DoPatch(SubmitDywhamHttpRequestParams httpRequestParams = null)
        {
            DoRequestAsync(new HttpMethod("PATCH"), httpRequestParams, CancellationToken.None).RunSynchronously();
        }

        public async Task DoPatchAsync(string url, object payload, CancellationToken ct)
        {
            await DoDeleteAsync(new SubmitDywhamHttpRequestParams { Url = url, Data = payload }, ct);
        }

        public async Task DoPatchAsync(SubmitDywhamHttpRequestParams httpRequestParams, CancellationToken ct)
        {
            await DoRequestAsync(new HttpMethod("PATCH"), httpRequestParams, ct);
        }

        public void DoPut(SubmitDywhamHttpRequestParams httpRequestParams = null)
        {
            DoRequestAsync(HttpMethod.Put, httpRequestParams, CancellationToken.None).RunSynchronously();
        }

        public async Task DoPutAsync(string url, CancellationToken ct)
        {
            await DoPostAsync(new SubmitDywhamHttpRequestParams { Url = url }, ct);
        }

        public async Task DoPutAsync(string url, object payload, CancellationToken ct)
        {
            await DoPutAsync(new SubmitDywhamHttpRequestParams { Url = url, Data = payload }, ct);
        }

        public async Task DoPutAsync(SubmitDywhamHttpRequestParams httpRequestParams, CancellationToken ct)
        {
            await DoRequestAsync(HttpMethod.Put, httpRequestParams, ct);
        }

        public T DoGet<T>(DywhamHttpRequestParams submitSettings)
        {
            return DoRequestAsync<T>(HttpMethod.Get, submitSettings, CancellationToken.None).Result;
        }

        public async Task<T> DoGetAsync<T>(string url, CancellationToken ct)
        {
            return await DoGetAsync<T>(new DywhamHttpRequestParams { Url = url }, ct);
        }

        public async Task<T> DoGetAsync<T>(DywhamHttpRequestParams submitSettings, CancellationToken ct)
        {
            return await DoRequestAsync<T>(HttpMethod.Get, submitSettings, ct);
        }

        public T DoGet<T>(DywhamHttpRequestParams submitSettings, Dictionary<string, object> requestHeaders)
        {
            return DoRequestAsync<T>(HttpMethod.Get, new DywhamHttpRequestParams { RequestHeaders = requestHeaders },
                CancellationToken.None).Result;
        }

        public async Task<T> DoGetAsync<T>(Dictionary<string, object> requestHeaders, CancellationToken ct)
        {
            return await DoRequestAsync<T>(HttpMethod.Get, new DywhamHttpRequestParams
                { RequestHeaders = requestHeaders }, ct);
        }

        public async Task<T> DoRequestAsync<T>(HttpMethod httpMethod, string url, CancellationToken ct)
        {
            return await DoRequestAsync<T>(httpMethod, new DywhamHttpRequestParams { Url = url }, ct);
        }

        public async Task<T> DoRequestAsync<T>(HttpMethod httpMethod, string url, object payload, CancellationToken ct)
        {
            return await DoRequestAsync<T>(httpMethod, new SubmitDywhamHttpRequestParams { Url = url, Data = payload }, ct);
        }

        public async Task DoRequestAsync(HttpMethod httpMethod, DywhamHttpRequestParams httpRequestParams, CancellationToken ct)
        {
            var url = httpRequestParams.QuerystringParams == null
                ? httpRequestParams.Url
                : ResolveRoute(httpRequestParams.Url, httpRequestParams.QuerystringParams);

            using (var requestMessage = new HttpRequestMessage { RequestUri = new Uri(url), Method = httpMethod })
            {
                var submitDywhamHttpRequestParams = httpRequestParams as SubmitDywhamHttpRequestParams;

                if (submitDywhamHttpRequestParams != null)
                {
                    PrepareHttpRequestMessage(requestMessage, submitDywhamHttpRequestParams.Data, submitDywhamHttpRequestParams.RequestHeaders,
                        submitDywhamHttpRequestParams.JsonSerializerSettingsOutgoing);
                }
                else
                {
                    PrepareHttpRequestMessage(requestMessage, null, httpRequestParams.RequestHeaders);
                }

                httpRequestParams.OnBeforeRequest?.Invoke(requestMessage);

                using (var httpResponseMessage = await HttpClient.SendAsync(requestMessage, ct))
                {
                    var message = string.Empty;

                    if (!httpResponseMessage.IsSuccessStatusCode)
                    {
                        message = await httpResponseMessage.Content.ReadAsStringAsync();

                        throw new ApiRequestException(httpResponseMessage.StatusCode, $"Status code: {httpResponseMessage.StatusCode} + {Environment.NewLine} + {message}");
                    }

                    httpRequestParams.OnAfterRequest?.Invoke(httpResponseMessage, message);
                }
            }
        }

        public async Task<T> DoRequestAsync<T>(HttpMethod httpMethod, DywhamHttpRequestParams httpRequestParams, CancellationToken ct)
        {
            var url = httpRequestParams.QuerystringParams == null
                ? httpRequestParams.Url
                : ResolveRoute(httpRequestParams.Url, httpRequestParams.QuerystringParams);

            using (var requestMessage = new HttpRequestMessage { RequestUri = new Uri(url), Method = httpMethod })
            {
                var submitDywhamHttpRequestParams = httpRequestParams as SubmitDywhamHttpRequestParams;

                if (submitDywhamHttpRequestParams != null)
                {
                    PrepareHttpRequestMessage(requestMessage, submitDywhamHttpRequestParams.Data, submitDywhamHttpRequestParams.RequestHeaders,
                        submitDywhamHttpRequestParams.JsonSerializerSettingsOutgoing);
                }
                else
                {
                    PrepareHttpRequestMessage(requestMessage, null, httpRequestParams.RequestHeaders);
                }

                httpRequestParams.OnBeforeRequest?.Invoke(requestMessage);

                using (var httpResponseMessage = await HttpClient.SendAsync(requestMessage, ct))
                {
                    using (var content = httpResponseMessage.Content)
                    {
                        if (!httpResponseMessage.IsSuccessStatusCode)
                        {
                            var messsage = await httpResponseMessage.Content.ReadAsStringAsync();

                            throw new ApiRequestException(httpResponseMessage.StatusCode,
                                $"Status code: {httpResponseMessage.StatusCode} + {Environment.NewLine} + {messsage}");
                        }

                        if (httpResponseMessage.StatusCode == HttpStatusCode.NoContent)
                        {
                            return default(T);
                        }

                        var message = content.ReadAsStringAsync().Result;

                        httpRequestParams.OnAfterRequest?.Invoke(httpResponseMessage, message);

                        return JsonConvert.DeserializeObject<T>(message, httpRequestParams.JsonSerializerSettingsIncoming);
                    }
                }
            }
        }

        private static void PrepareHttpRequestMessage(HttpRequestMessage httpRequestMessage, object data,
            Dictionary<string, object> requestHeaders, JsonSerializerSettings serializerSettings = null)
        {
            var serializedData = string.Empty;

            if (data != null)
            {
                serializedData = serializerSettings == null
                    ? JsonConvert.SerializeObject(data)
                    : JsonConvert.SerializeObject(data, serializerSettings);
            }

            if (!string.IsNullOrWhiteSpace(serializedData))
            {
                httpRequestMessage.Content = new StringContent(serializedData, Encoding.UTF8, "application/json");
            }

            if (requestHeaders == null || !requestHeaders.Any()) return;

            foreach (var key in requestHeaders.Keys)
            {
                httpRequestMessage.Headers.TryAddWithoutValidation(key, requestHeaders[key].ToString());
            }
        }

        private static string ResolveRoute(string route, object payload)
        {
            var querystringParams = payload.GetType().GetRuntimeProperties()
                .Where(x => x.GetValue(payload) != null
                    && !string.IsNullOrWhiteSpace(x.GetValue(payload).ToString()))
                .ToDictionary(x => x, x => x.GetValue(payload));
            var uriBuilder = new StringBuilder(new UriBuilder(new Uri($"{route}").AbsoluteUri).ToString());

            if (!querystringParams.Any())
            {
                return uriBuilder.ToString();
            }

            uriBuilder.Append("?");

            foreach (var param in querystringParams.Where(x => x.Value != null))
            {
                if (typeof(IList).IsAssignableFrom(param.Key.PropertyType))
                {
                    foreach (var arrayParams in (IList)param.Value)
                    {
                        uriBuilder.Append($"{param.Key.Name}={arrayParams}&");
                    }

                    uriBuilder = new StringBuilder(uriBuilder.ToString().Substring(0, uriBuilder.Length - 1));

                    continue;
                }

                uriBuilder.Append($"{param.Key.Name}={param.Value}&");
            }

            uriBuilder = new StringBuilder(uriBuilder.ToString().Substring(0, uriBuilder.Length - 1));

            return uriBuilder.ToString();
        }

        public void Dispose()
        {
            HttpClient?.Dispose();
        }
    }
}