﻿using System;
using System.Threading;
using Common.Logging;
using Dywham.Middleware.Endpoints.Bus.Contracts;

namespace Dywham.Middleware.Endpoints.Bus.Handling
{
    public class DywhamHandlingExecutionContext : IDywhamHandlingExecutionContext
    {
        public IDywhamBus DywhamBus { get; internal set; }

        public ILog Log { get; internal set; }

        public DateTime ExecutionStartDateTime { get; internal set; }

        public CancellationToken CancellationToken { get; internal set; }
    }
}