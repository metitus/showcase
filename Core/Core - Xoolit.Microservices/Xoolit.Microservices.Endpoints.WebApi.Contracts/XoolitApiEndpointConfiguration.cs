﻿using Dywham.Middleware.Endpoints.WebApi.Contracts;

namespace Xoolit.Microservices.Endpoints.WebApi.Contracts
{
    public class XoolitApiEndpointConfiguration : ApiEndpointConfiguration
    {
        public int SessionTimeout { get; set; }
    }
}
