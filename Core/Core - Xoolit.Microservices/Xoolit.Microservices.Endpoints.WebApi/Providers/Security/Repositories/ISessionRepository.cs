﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Dywham.Middleware.Storage.Repositories.Contracts;
using Xoolit.Microservices.Endpoints.WebApi.Providers.Security.Repositories.Entities;

namespace Xoolit.Microservices.Endpoints.WebApi.Providers.Security.Repositories
{
    public interface ISessionRepository : IRepository<SessionEntity>
    {
        Task<SessionEntity> GetAsync(Guid sessionId, CancellationToken token);
    }
}