﻿CREATE TABLE [dbo].[Security.Resources] (
    [Pattern]              NVARCHAR (300) NOT NULL,
    [ResourceAccessModeId] INT            NOT NULL,
    [IsActive]             BIT            NOT NULL,
    CONSTRAINT [PK_Resources] PRIMARY KEY CLUSTERED ([Pattern] ASC),
    CONSTRAINT [FK_Security.Resources_ResourceAccessModes] FOREIGN KEY ([ResourceAccessModeId]) REFERENCES [dbo].[ResourceAccessModes] ([Id])
);



