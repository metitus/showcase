﻿using System.Collections.Generic;

namespace Dywham.Middleware.Utils.Collections
{
    public class ResultSet<T> where T : class
    {
        public IEnumerable<T> Set { get; set; }

        public long TotalCount { get; set; }
    }
}