﻿using System.Data.Entity;
using Dywham.Middleware.Providers.EventStore.EF.Repositories.Entities;
using Dywham.Middleware.Storage.Repositories;

namespace Dywham.Middleware.Providers.EventStore.EF.Repositories
{
    public class EventStoreDatabaseContextContext : DatabaseContext
    {
        public EventStoreDatabaseContextContext(string dataSource) : base(dataSource)
        { }


        public virtual DbSet<EventEntity> Events { get; set; }
    }
}