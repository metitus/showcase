﻿using System;

namespace Dywham.Middleware.Endpoints
{
    public class EndpointRunModeException : Exception
    {
        public EndpointRunModeException() : base("Cannot run internal messages while on release mode")
        { }
    }
}