﻿CREATE TABLE [dbo].[ResourceAccessModes] (
    [Id]   INT           NOT NULL,
    [Name] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ResourceAccessModes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

