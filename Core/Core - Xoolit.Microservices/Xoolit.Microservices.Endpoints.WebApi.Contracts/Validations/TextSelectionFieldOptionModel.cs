﻿namespace Xoolit.Microservices.Endpoints.WebApi.Contracts.Validations
{
    public class TextSelectionFieldOptionModel
    {
        public string Display { get; set; }

        public string Option { get; set; }
    }
}
