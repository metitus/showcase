﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Dywham.Middleware.Storage.Repositories.Contracts;
using Xoolit.Microservices.Accounts.Repositories.Entities;

namespace Xoolit.Microservices.Accounts.Repositories
{
    public interface ISessionRepository : IRepository<SessionEntity>
    {
        Task<SessionEntity> GetSingleAsync(Guid id, CancellationToken token);

        Task<IList<SessionEntity>> GetByAsync(SessionEntityQueryParms query, CancellationToken token);

        Task AddAsync(SessionEntity session, CancellationToken token);

        Task UpdateAsync(SessionEntity session, CancellationToken token);

        Task UpdateBulkAsync(Func<SessionEntity, bool> condition, SessionEntity newEntity, CancellationToken token);
    }
}