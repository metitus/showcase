﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Xoolit.Microservices.Endpoints.WebApi.Providers.Security.Repositories.Entities
{
    [Table("Resources")]
    public class ResourceEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Pattern { get; set; }

        public int ResourceAccessModeId { get; set; }

        public bool IsActive { get; set; }
    }
}