﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Dywham.Middleware.Utils
{
    public static class AssemblyUtils
    {
        private static Assembly[] _assemblies;


        public static Assembly[] Assemblies(string path)
        {
            if (_assemblies != null)
            {
                return _assemblies;
            }

            if (string.IsNullOrEmpty(path))
            {
                path = AppDomain.CurrentDomain.BaseDirectory;
            }

            // ReSharper disable once AssignNullToNotNullAttribute
            var files = Directory.GetFiles(path, "*.dll", SearchOption.AllDirectories).ToList();
            var assembliesTemp = new List<Assembly>();

            foreach (var file in files)
            {
                try
                {
                    var fileName = Path.GetFileName(file);

                    if (string.IsNullOrEmpty(fileName)) continue;

                    assembliesTemp.Add(Assembly.LoadFrom(file));
                }
                catch (FileLoadException)
                { } // The Assembly has already been loaded.
                catch (BadImageFormatException)
                { } // If a BadImageFormatException exception is thrown, the file is not an assembly.
            }

            _assemblies = assembliesTemp.ToArray();

            return _assemblies;
        }

        public static IList<Type> GetTypesMatching(string defaultExecutionPath, Func<Type, bool> condition)
        {
            var types = new List<Type>();

            foreach (var assembly in Assemblies(defaultExecutionPath))
            {
                try
                {
                    var typesToAdd = assembly.GetTypes().Where(condition).ToList();

                    if (!typesToAdd.Any()) continue;

                    types.AddRange(typesToAdd);
                }
                catch
                {
                    // ignored
                }
            }

            return types;
        }


        public static Type[] GetAllTypesExceptSubtypesOf(this Assembly[] assemblies, Type typeToExclude)
        {
            var types = new List<Type>();

            foreach (var assembly in assemblies)
            {
                try
                {
                    types.AddRange(assembly.GetTypes().Where(type => !typeToExclude.IsAssignableFrom(type)));
                }
                catch
                {
                    // ignored
                }
            }

            return types.ToArray();
        }
    }
}