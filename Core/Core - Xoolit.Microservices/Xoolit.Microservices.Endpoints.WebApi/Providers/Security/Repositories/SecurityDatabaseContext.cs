﻿using System.Data.Entity;
using Dywham.Middleware.Storage.Repositories;
using Dywham.Middleware.Storage.Repositories.Contracts;
using Xoolit.Microservices.Endpoints.WebApi.Providers.Security.Repositories.Entities;

namespace Xoolit.Microservices.Endpoints.WebApi.Providers.Security.Repositories
{
    public class SecurityDatabaseContext : DatabaseContext, IDatabaseContextFactory<SecurityDatabaseContext>
    {
        public SecurityDatabaseContext()
        { }

        public SecurityDatabaseContext(string dataSource) : base(dataSource)
        { }


        public IXoolitSecurityProviderSettings XoolitSecurityProviderSettings { get; set; }

        public virtual DbSet<SessionEntity> Sessions { get; set; }

        public virtual DbSet<ResourceEntity> Resources { get; set; }


        public SecurityDatabaseContext CreateInstance(string dataSource)
        {
            return new SecurityDatabaseContext(XoolitSecurityProviderSettings.SecurityDataSource);
        }
    }
}