﻿namespace Xoolit.Microservices.Accounts.Providers.PasswordHashing
{
    public interface IPasswordHashingProvider
    {
        string CreateHash(string password);

        string CreateHash(string password, int iterations);

        bool Verify(string password, string hashedPassword);
    }
}