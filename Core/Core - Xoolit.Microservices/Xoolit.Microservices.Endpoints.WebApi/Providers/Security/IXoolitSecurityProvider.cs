﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using Dywham.Middleware.Endpoints.WebApi.Contracts;
using Dywham.Middleware.Endpoints.WebApi.Contracts.Providers;

namespace Xoolit.Microservices.Endpoints.WebApi.Providers.Security
{
    public interface IXoolitSecurityProvider : ISecurityProvider
    {
        Task<PermissonExecutionResult> ValidateSessionAsync(HttpActionContext actionContext,
            HttpRequestMessage httpRequestMessage, CancellationToken token);
    }
}