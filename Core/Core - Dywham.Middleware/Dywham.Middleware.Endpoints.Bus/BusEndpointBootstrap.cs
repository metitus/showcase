﻿using System;
using System.Reflection;
using Autofac;
using Dywham.Middleware.Endpoints.Bus.Contracts;
using Dywham.Middleware.Endpoints.Bus.Handling;
using Dywham.Middleware.Endpoints.NServiceBus;
using Dywham.Middleware.Providers.EventStore.Contracts;
using Dywham.Middleware.Utils;
using NServiceBus;
using NServiceBus.Logging;
using Dywham.Middleware.Endpoints.Bus.Contracts.Jobs;

namespace Dywham.Middleware.Endpoints.Bus
{
    public abstract class BusEndpointBootstrap : EndpointBootstrap, IConfigureThisEndpoint
    {
        private static readonly Common.Logging.ILog Logger = Common.Logging.LogManager.GetLogger(typeof(EndpointUtils));


        protected EndpointExecutionMode ExecutionMode = EndpointExecutionMode.Release;

        protected string AuditQueue = string.Empty;

        protected string ErrorQueue = "Error";

        protected bool EnableInstallers = true;

        protected static bool EnableJobScheduler;

        protected string PersistanceConnectionString;


        public void Customize(EndpointConfiguration endpointConfiguration)
        {
            try
            {
                LogManager.Use<CommonLoggingFactory>();

                DefaultExecutionPath = AppDomain.CurrentDomain.BaseDirectory;

                ContainerBuilder = new ContainerBuilder();

                OnEndpointConfigurationStart(endpointConfiguration, ContainerBuilder);

                InitializeDiContainer(ContainerBuilder);

                SetSettings();

                endpointConfiguration.DefineEndpointName($"Microservices.{GetType().Name.Substring(0, GetType().Name.IndexOf("Endpoint", StringComparison.OrdinalIgnoreCase))}");

                if (!string.IsNullOrWhiteSpace(EndpointName))
                {
                    endpointConfiguration.DefineEndpointName(EndpointName);
                }

                var container = ContainerBuilder.Build();

                if (!string.IsNullOrWhiteSpace(ErrorQueue)) endpointConfiguration.SendFailedMessagesTo(ErrorQueue);

                if (!string.IsNullOrWhiteSpace(AuditQueue)) endpointConfiguration.AuditProcessedMessagesTo(AuditQueue);

                if (EnableInstallers) endpointConfiguration.EnableInstallers();

                ConfigureMessagesConventions(endpointConfiguration);

                LoadMappings();

                endpointConfiguration.UseContainer<AutofacBuilder>(customizations =>
                {
                    customizations.ExistingLifetimeScope(container);
                });

                OnEndpointConfigurationCompleted(endpointConfiguration, container);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);

                var exception = ex as ReflectionTypeLoadException;

                if (exception == null) throw;

                foreach (var loaderException in exception.LoaderExceptions)
                {
                    Logger.Error(loaderException.Message, loaderException);
                }

                throw;
            }
        }

        private ContainerBuilder InitializeDiContainer(ContainerBuilder builder)
        {
            var assemblies = AssemblyUtils.Assemblies(DefaultExecutionPath);

            builder.RegisterType<DywhamHandlingExecution>().As<IDywhamHandlingExecution>()
                .PropertiesAutowired()
                .SingleInstance();

            builder.RegisterAssemblyTypes(assemblies)
                .Where(t => typeof(IEventStoreProvider).IsAssignableFrom(t))
                .AsImplementedInterfaces()
                .PropertiesAutowired();

            builder.Register(ctx => new DywhamEndpointBusConfiguration
            {
                ExecutionMode = ExecutionMode,
                AuditQueue = AuditQueue,
                ErrorQueue = ErrorQueue,
                EndpointName = EndpointName,
                EnableInstallers = EnableInstallers,
                EnableJobScheduler = EnableJobScheduler
            }).As<DywhamEndpointBusConfiguration>().SingleInstance().PropertiesAutowired();

            if (EnableJobScheduler)
            {
                builder.RegisterAssemblyTypes(assemblies)
                    .Where(t => typeof(DywhamJob).IsAssignableFrom(t))
                    .As<DywhamJob>()
                    .PropertiesAutowired();
            }

            builder.RegisterAssemblyModules(assemblies);

            return builder;
        }

        protected virtual void ConfigureMessagesConventions(EndpointConfiguration endpointConfiguration)
        {
            NServiceBusEndpointUtils.ConfigureMessagesConventions(endpointConfiguration);
        }

        protected abstract void OnEndpointConfigurationStart(EndpointConfiguration configuration, ContainerBuilder builder);

        protected virtual void OnEndpointConfigurationCompleted(EndpointConfiguration configuration, IContainer container)
        { }
    }

    public abstract class BusEndpointBootstrap<T> : BusEndpointBootstrap where T : SettingsForBusEndpoint
    {
        public T EndpointSettings { get; set; }


        protected override void SetSettings()
        {
            var settings = EndpointUtils.LoadComponentsConfigurationFiles<SettingsForBusEndpoint>(typeof(T),
                AppDomain.CurrentDomain.BaseDirectory, this, ContainerBuilder);

            EndpointUtils.SetSettings(this, settings);

            if (!string.IsNullOrWhiteSpace(settings.ErrorQueue))
            {
                ErrorQueue = settings.ErrorQueue;
            }

            if (!string.IsNullOrWhiteSpace(settings.PersistanceConnectionString))
            {
                PersistanceConnectionString = settings.PersistanceConnectionString;
            }
        }
    }
}