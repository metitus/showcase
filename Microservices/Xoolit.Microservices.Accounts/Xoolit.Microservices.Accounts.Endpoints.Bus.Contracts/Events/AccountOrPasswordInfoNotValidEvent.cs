using Dywham.Middleware.Endpoints.Bus.Contracts.Messages.Events;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Events
{
    public class AccountOrPasswordInfoNotValidEvent : MiddlewareEvent
    { }
}