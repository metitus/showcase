﻿namespace Xoolit.Microservices.Endpoints.WebApi.Contracts.Validations
{
    public class TextFieldModel : FieldModel
    {
        public static TextFieldModel Default = new TextFieldModel();

        public int? MinLength { get; set; }

        public int? MaxLength { get; set; }

        public string Regex { get; set; }
    }
}