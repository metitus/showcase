﻿namespace Dywham.Middleware.Endpoints
{
    public enum EndpointExecutionMode
    {
        Release,
        Internal
    }
}