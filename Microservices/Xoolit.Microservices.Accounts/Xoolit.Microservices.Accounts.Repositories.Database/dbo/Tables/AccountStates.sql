﻿CREATE TABLE [dbo].[AccountStates] (
    [Id]   INT           NOT NULL,
    [Name] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_AccountStates] PRIMARY KEY CLUSTERED ([Id] ASC)
);

