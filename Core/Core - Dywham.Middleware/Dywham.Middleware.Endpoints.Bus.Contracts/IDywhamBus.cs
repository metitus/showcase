﻿using System.Threading;
using System.Threading.Tasks;
using Dywham.Middleware.Endpoints.Bus.Contracts.Messages.Commands;
using Dywham.Middleware.Endpoints.Bus.Contracts.Messages.Events;

namespace Dywham.Middleware.Endpoints.Bus.Contracts
{
    public interface IDywhamBus
    {
        Task SendAsync(MiddlewareCommand message, CancellationToken token);

        Task SendLocalAsync(MiddlewareCommand message, CancellationToken token);

        Task<MiddlewareEvent> PublishAsync(MiddlewareEvent message, CancellationToken token, bool storeEvent = true);

        Task<MiddlewareEvent> PublishAsync(MiddlewareEvent message, MiddlewareCommand command, CancellationToken token, bool storeEvent = true);
    }
}
