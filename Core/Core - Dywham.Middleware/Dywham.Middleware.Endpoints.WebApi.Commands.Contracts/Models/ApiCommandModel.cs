using System;

namespace Dywham.Middleware.Endpoints.WebApi.Commands.Contracts.Models
{
    public class ApiCommandModel
    {
        public Guid? MessageId { get; set; }

        public Guid? RequestedByTrackerId { get; set; }

        public DateTime? DateTime { get; set; } = System.DateTime.UtcNow;
    }
}