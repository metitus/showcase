﻿using System;

namespace Xoolit.Microservices.Endpoints.WebApi.Contracts.Security
{
    [AttributeUsage(AttributeTargets.Method)]
    public class SecureRouteAttribute : Attribute
    {
        public SecureRouteAttribute(ResourceAccessMode resourceAccessMode)
        {
            ResourceAccessMode = resourceAccessMode;
        }


        public ResourceAccessMode ResourceAccessMode { get; }
    }
}
