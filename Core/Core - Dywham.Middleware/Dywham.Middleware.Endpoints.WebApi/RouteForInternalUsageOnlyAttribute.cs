﻿using System;

namespace Dywham.Middleware.Endpoints.WebApi
{
    [AttributeUsage(AttributeTargets.Method)]
    public class RouteForInternalUsageOnlyAttribute : Attribute
    { }
}