﻿CREATE TABLE [dbo].[Accounts] (
    [Id]                  BIGINT           IDENTITY (1, 1) NOT NULL,
    [ExternalId]          UNIQUEIDENTIFIER NOT NULL,
    [FirstName]           NVARCHAR (10)    NOT NULL,
    [Surname]             NVARCHAR (10)    NOT NULL,
    [Username]            NVARCHAR (25)    NOT NULL,
	[PasswordHash]		  NVARCHAR (75)    NOT NULL,
    [Email]               NVARCHAR (254)   NOT NULL,
    [Mobile]              NVARCHAR (50)    NULL,
    [EmailActivationCode] NVARCHAR (100)   NOT NULL,
    [SmsActivationCode]	  INT			       NULL,
    [AccountStateId]      INT              NOT NULL,
    [DateTimeCreated]     DATETIME2 (7)    NOT NULL,
    [DateTimeLastChanged] DATETIME2 (7)    NOT NULL,
    [Version]             INT              NOT NULL,
    CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Accounts_AccountStates] FOREIGN KEY ([AccountStateId]) REFERENCES [dbo].[AccountStates] ([Id]),
    CONSTRAINT [IX_Accounts_1] UNIQUE NONCLUSTERED ([Email] ASC),
    CONSTRAINT [IX_Accounts_2] UNIQUE NONCLUSTERED ([Username] ASC)
);



