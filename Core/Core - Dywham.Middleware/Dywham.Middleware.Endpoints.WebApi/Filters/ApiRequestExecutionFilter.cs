﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.Routing;
using Newtonsoft.Json;
using Common.Logging;
using Dywham.Middleware.Endpoints.WebApi.Contracts;
using Dywham.Middleware.Endpoints.WebApi.Contracts.Models;
using Dywham.Middleware.Endpoints.WebApi.Contracts.Providers;

namespace Dywham.Middleware.Endpoints.WebApi.Filters
{
    public class ApiRequestExecutionFilter : ActionFilterAttribute, IExceptionFilter
    {
        private string _route;
        private string _method;
        private object _payload;
        private Dictionary<string, object> _requestValues;


        public IExecutionTracer ExecutionTracer { get; set; }


        public override async Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            await TraceExecutionStartAsync(actionContext, cancellationToken);

            await VerifySecurityAsync(actionContext, cancellationToken);

            await PrepareRequestAsync(actionContext, cancellationToken);
        }

        public override async Task OnActionExecutedAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            await PrepareResponseAsync(actionExecutedContext, cancellationToken);

            await TraceExecutionEndAsync(actionExecutedContext, cancellationToken);
        }

        public Task ExecuteExceptionFilterAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() =>
            {
                LogRequestInfo(actionExecutedContext.ActionContext);

                var logger = LogManager.GetLogger(actionExecutedContext.ActionContext.ControllerContext.Controller.GetType());

                logger.Error(actionExecutedContext.Exception);

                var httpStatusCode = GetHttpStatusCode(actionExecutedContext.Exception);

                actionExecutedContext.Response = actionExecutedContext.Request
                    .CreateResponse(httpStatusCode);
            }, cancellationToken);
        }

        public Task PrepareRequestAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            var apiEndpointConfiguration = (ApiEndpointConfiguration)actionContext
                .Request.GetDependencyScope().GetService(typeof(ApiEndpointConfiguration));

            if (apiEndpointConfiguration.ExecutionMode == EndpointExecutionMode.Release &&
                actionContext.ActionDescriptor.GetCustomAttributes<RouteForInternalUsageOnlyAttribute>().Any())
            {
                throw new EndpointRunModeException();
            }

            if (!actionContext.ModelState.IsValid)
            {
                actionContext.Response = actionContext.Request
                    .CreateErrorResponse(HttpStatusCode.BadRequest, actionContext.ModelState);

                return Task.CompletedTask;
            }

            var model = actionContext.ActionArguments.Values.FirstOrDefault(v => v is IModelValidator);

            if (model == null)
            {
                return base.OnActionExecutingAsync(actionContext, cancellationToken);
            }

            var modelValidatorType = typeof(IModelValidator<>).MakeGenericType(model.GetType());
            var modelValidator = actionContext.Request.GetDependencyScope().GetService(modelValidatorType);

            if (modelValidator == null)
            {
                return base.OnActionExecutingAsync(actionContext, cancellationToken);
            }

            var validationResult = (ValidationResult)modelValidator.GetType().InvokeMember("Validate",
                BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public, null, modelValidator,
                new[] { model, actionContext.Request });

            if (validationResult == null || !validationResult.ValidationIssues.Any())
            {
                return base.OnActionExecutingAsync(actionContext, cancellationToken);
            }

            actionContext.Response = actionContext.Request
                .CreateErrorResponse(HttpStatusCode.BadRequest, JsonConvert.SerializeObject(validationResult));

            return Task.CompletedTask;
        }

        public Task PrepareResponseAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            if (actionExecutedContext.Exception != null || actionExecutedContext.ActionContext.ActionDescriptor.GetCustomAttributes<CustomResponseAttribute>().Any())
            {
                return base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
            }

            if (actionExecutedContext.Request.Method == HttpMethod.Get)
            {
                var content = actionExecutedContext.Response.Content as ObjectContent;

                if (content == null) return base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);

                var objectContent = content;

                if (!objectContent.ObjectType.IsAssignableFrom(typeof(IEnumerable)) && objectContent.Value == null)
                {
                    actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            else
            {
                actionExecutedContext.Response = actionExecutedContext.Request
                    .CreateResponse(actionExecutedContext.Response.Content == null
                        ? HttpStatusCode.Accepted
                        : HttpStatusCode.BadRequest);
            }

            return base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
        }

        private async Task TraceExecutionStartAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            _route = actionContext.Request.RequestUri.AbsolutePath;
            _method = actionContext.Request.Method.Method;
            _requestValues = actionContext.ActionArguments;

            if (_requestValues.Values.Where(x => x != null).Any(x => x.GetType().Name.EndsWith("Model")))
            {
                _payload = _requestValues.Values.FirstOrDefault(x => x.GetType().Name.EndsWith("Model"));
            }

            ExecutionTracer = (IExecutionTracer)actionContext.Request.GetDependencyScope().GetService(typeof(IExecutionTracer)) ??
                              new EmptyExecutionTracer();

            await ExecutionTracer.BeforeHandlingAsync(_route, _method, _payload, _requestValues, cancellationToken);

            LogRequestInfo(actionContext);

            await base.OnActionExecutingAsync(actionContext, cancellationToken);
        }

        private async Task TraceExecutionEndAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            var logger = LogManager.GetLogger(actionExecutedContext.ActionContext.ControllerContext.Controller.GetType());

            if (actionExecutedContext.Exception != null)
            {
                await ExecutionTracer.OnExceptionAsync(_route, _method, _payload, _requestValues,
                    actionExecutedContext.Exception, cancellationToken);

                logger.Error(actionExecutedContext.Exception.Message, actionExecutedContext.Exception);
            }
            else
            {
                await ExecutionTracer.AfterHandlingAsync(_route, _method, _payload, _requestValues, cancellationToken);
            }

            await base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
        }

        private void LogRequestInfo(HttpActionContext actionContext)
        {
            var logger = LogManager.GetLogger(actionContext.ControllerContext.Controller.GetType());

            logger.Info($"{_method} {_route}");

            if (actionContext.Request.Method != HttpMethod.Post && actionContext.Request.Method != HttpMethod.Put) return;

            if (_requestValues.Values.Any(x => x == null))
            {
                logger.Info("One of the ActionArguments is null. Verify the any message payload being sent.");
            }

            if (_payload != null)
            {
                logger.Info(JsonConvert.SerializeObject(_payload));
            }
        }

        private static HttpStatusCode GetHttpStatusCode(Exception exception)
        {
            var httpStatusCode = HttpStatusCode.InternalServerError;
            var httpException = exception as HttpException;

            if (httpException == null) return httpStatusCode;

            var httpCode = httpException.GetHttpCode();

            if (Enum.IsDefined(typeof(HttpStatusCode), httpCode))
            {
                httpStatusCode = (HttpStatusCode)httpCode;
            }

            return httpStatusCode;
        }

        private async Task VerifySecurityAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            var request = actionContext.ControllerContext.Request;

            if (request.GetConfiguration().Routes.GetRouteData(request) == null)
            {
                await base.OnActionExecutingAsync(actionContext, cancellationToken);
            }

            var routedData = request.GetConfiguration().Routes.GetRouteData(request);

            if (!routedData.Values.ContainsKey("MS_SubRoutes"))
            {
                await base.OnActionExecutingAsync(actionContext, cancellationToken);
            }

            var httpResponseMessage = new HttpResponseMessage();
            var routeTemplate = ((IHttpRouteData[])routedData.Values["MS_SubRoutes"]).First().Route.RouteTemplate;

            if (await ValidateResourceAccessAsync(actionContext, httpResponseMessage, request, routeTemplate, cancellationToken))
            {
                await base.OnActionExecutingAsync(actionContext, cancellationToken);
            }
            else
            {
                actionContext.Response = httpResponseMessage;
            }
        }

        private static async Task<bool> ValidateResourceAccessAsync(HttpActionContext actionContext, HttpResponseMessage httpResponseMessage,
            HttpRequestMessage request, string routeTemplate, CancellationToken cancellationToken)
        {
            var securityProvider = (ISecurityProvider)request.GetDependencyScope().GetService(typeof(ISecurityProvider));

            if (securityProvider == null) return true;

            var executionPermission = await securityProvider.ValidateResourcePermissionAsync(actionContext, routeTemplate, request, cancellationToken);

            if (executionPermission.Status != ExecutionPermissionStatus.Allowed)
            {
                httpResponseMessage.StatusCode = executionPermission.HttpStatusCode;
                httpResponseMessage.Content = new StringContent(" ");
            }
            else
            {
                var apiRequestMutator = (IApiRequestMutatorProvider)request.GetDependencyScope().GetService(typeof(IApiRequestMutatorProvider));

                apiRequestMutator?.MutateRequest(routeTemplate, request.Headers);

                return true;
            }

            return false;
        }
    }
}