﻿using System;

namespace Xoolit.Microservices.Accounts.Repositories.Entities
{
    public class SessionEntityQueryParms
    {
        public DateTime? DateTimeStarted { get; set; }

        public DateTime? DateTimeFinished { get; set; }

        public DateTime? LastDateTimePing { get; set; }

        public string IpAddress { get; set; }

        public int? SessionFinishedTypeId { get; set; }
    }
}