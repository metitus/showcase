﻿using NServiceBus;

namespace Dywham.Middleware.Endpoints.WebApi.Commands
{
    public class ApiCommandsEndpoint : ApiEndpoint
    {
        public IEndpointInstance Bus { get; set; }
    }
}