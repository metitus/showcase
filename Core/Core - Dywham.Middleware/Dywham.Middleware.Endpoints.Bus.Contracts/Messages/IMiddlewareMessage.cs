﻿using System;

namespace Dywham.Middleware.Endpoints.Bus.Contracts.Messages
{
    public interface IMiddlewareMessage
    {
        Guid MessageId { get; set; }

        DateTime DateTime { get; set; }

        Guid RequestedByTrackerId { get; set; }

        Guid GeneratedInTheContextOf { get; set; }
    }
}