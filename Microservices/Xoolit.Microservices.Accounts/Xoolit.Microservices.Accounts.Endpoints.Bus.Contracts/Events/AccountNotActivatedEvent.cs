﻿using Dywham.Middleware.Endpoints.Bus.Contracts.Messages.Events;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Events
{
    public class AccountNotActivatedEvent : MiddlewareEvent
    {
        public string ActivationCode { get; set; }

        public AccountNotActivatedReason Reason { get; set; }
    }
}