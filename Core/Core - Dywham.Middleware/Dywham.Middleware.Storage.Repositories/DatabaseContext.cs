﻿using System.Data.Entity;
using System.Linq;
using System.Reflection;
using Dywham.Middleware.Storage.Repositories.Contracts;

namespace Dywham.Middleware.Storage.Repositories
{
    public abstract class DatabaseContext : DbContext
    {
        protected DatabaseContext()
        { }

        protected DatabaseContext(string dataSource) : base(dataSource)
        {
            Configuration.LazyLoadingEnabled = true;

            Init();
        }


        private void Init()
        {
            var properties = GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(x => typeof(IRepository).IsAssignableFrom(x.PropertyType));

            foreach (var property in properties)
            {
                var instance = property.GetValue(this, null);
                var method = instance.GetType().GetMethod("UseDbContext");

                method?.Invoke(instance, new object[] { this });
            }
        }
    }
}