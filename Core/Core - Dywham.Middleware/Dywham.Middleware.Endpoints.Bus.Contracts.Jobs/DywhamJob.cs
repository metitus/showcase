﻿using NServiceBus;
using Quartz;

namespace Dywham.Middleware.Endpoints.Bus.Contracts.Jobs
{
    public abstract class DywhamJob : IJob
    {
        public abstract IJobDetail JobDetail { get; }

        public abstract ITrigger Trigger { get; }


        public virtual void Execute(IJobExecutionContext context, IMessageSession messageSession)
        { }

        public void Execute(IJobExecutionContext context)
        {
            Execute(context, (IMessageSession) context.Scheduler.Context.Get("MessageSession"));
        }
    }
}