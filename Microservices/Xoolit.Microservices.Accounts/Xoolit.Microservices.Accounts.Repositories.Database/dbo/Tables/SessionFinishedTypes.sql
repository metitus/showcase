﻿CREATE TABLE [dbo].[SessionFinishedTypes] (
    [Id]   INT        NOT NULL,
    [Name] NVARCHAR(50) NULL,
    CONSTRAINT [PK_SessionFinishedType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

