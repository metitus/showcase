﻿using System;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using Dywham.Middleware.Storage.Repositories;
using Dywham.Middleware.Storage.Repositories.Contracts;
using Xoolit.Microservices.Accounts.Repositories.Entities;

namespace Xoolit.Microservices.Accounts.Repositories
{
    public class AccountRepository : Repository<AccountEntity, AccountsDatabaseContext>, IAccountRepository
    {
        public AccountRepository(IRepositoriesSettings setting, IDatabaseContextFactory<AccountsDatabaseContext> databaseContextFactory)
            : base(setting, databaseContextFactory)
        { }


        public async Task<AccountEntity> GetAsync(Guid id, CancellationToken token)
        {
            return await DbContext.Accounts.FirstOrDefaultAsync(x => x.ExternalId == id, token);
        }

        public async Task<AccountEntity> GetByEmailAsync(string email, CancellationToken token)
        {
            return await DbContext.Accounts
                .FirstOrDefaultAsync(x => x.Email.Equals(email, StringComparison.OrdinalIgnoreCase), token);
        }

        public async Task<AccountEntity> GetByUsernameOrEmailAsync(string username, string email, CancellationToken token)
        {
            return await DbContext.Accounts
                .FirstOrDefaultAsync(x => x.Username.Equals(username, StringComparison.Ordinal) ||
                    x.Email.Equals(email, StringComparison.OrdinalIgnoreCase), token);
        }


        public async Task<AccountEntity> GetByActivationAsync(string activationCode, CancellationToken token)
        {
            return await DbContext.Accounts.AsNoTracking().FirstOrDefaultAsync(x =>
                x.EmailActivationCode.Equals(activationCode, StringComparison.OrdinalIgnoreCase), token);
        }

        public async Task AddAsync(AccountEntity account, CancellationToken token)
        {
            DbContext.Accounts.Add(account);

            await DbContext.SaveChangesAsync(token);
        }


        public async Task UpdateAsync(AccountEntity account, CancellationToken token)
        {
            DbContext.Entry(account).State = EntityState.Modified;

            await DbContext.SaveChangesAsync(token);
        }
    }
}