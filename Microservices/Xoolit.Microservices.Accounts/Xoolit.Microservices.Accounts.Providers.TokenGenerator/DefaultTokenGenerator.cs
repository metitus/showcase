﻿using System;

namespace Xoolit.Microservices.Accounts.Providers.TokenGenerator
{
    public class DefaultTokenGenerator : ITokenGenerator
    {
        public string GenerateEmailToken()
        {
            return Guid.NewGuid().ToString();
        }

        public int GenerateSmsToken()
        {
            const int min = 1000;
            const int max = 9999;
            var rdm = new Random();

            return rdm.Next(min, max);
        }
    }
}