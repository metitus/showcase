﻿using System.Collections.Generic;

namespace Xoolit.Microservices.Endpoints.WebApi.Contracts.Validations
{
    public class NumericSelectionFieldModel : FieldModel
    {
        public IList<NumericSelectionFieldOptionModel> Options { get; set; }
    }
}