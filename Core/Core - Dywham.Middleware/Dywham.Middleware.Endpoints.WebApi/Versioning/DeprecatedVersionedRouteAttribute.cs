﻿using System;

namespace Dywham.Middleware.Endpoints.WebApi.Versioning
{
    [AttributeUsage(AttributeTargets.Method)]
    public class DeprecatedVersionedRouteAttribute : VersionedRouteAttribute
    {
        public DeprecatedVersionedRouteAttribute(string template, int allowedVersion, string deprecatedMessage, int newVersion)
    :   base(template, allowedVersion)
        {
            DeprecatedMessage = deprecatedMessage;

            NewVersion = newVersion;

            StillSupported = true;
        }

        public DeprecatedVersionedRouteAttribute(string template, int allowedVersion, string deprecatedMessage, int newVersion, bool stillSupported)
            : base(template, allowedVersion)
        {
            DeprecatedMessage = deprecatedMessage;

            NewVersion = newVersion;

            StillSupported = stillSupported;
        }


        public string DeprecatedMessage { get; }

        public int NewVersion { get; }

        public bool StillSupported { get; }
    }
}