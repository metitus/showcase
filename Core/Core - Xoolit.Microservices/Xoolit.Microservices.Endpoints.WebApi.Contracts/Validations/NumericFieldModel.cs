﻿namespace Xoolit.Microservices.Endpoints.WebApi.Contracts.Validations
{
    public class NumericFieldModel : FieldModel
    {
        public static NumericFieldModel Default = new NumericFieldModel();

        public int? Min { get; set; }

        public int? Max { get; set; }

        public string Regex { get; set; }
    }
}