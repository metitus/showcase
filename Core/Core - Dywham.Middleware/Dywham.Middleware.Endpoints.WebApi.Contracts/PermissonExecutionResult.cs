﻿using System.Net;

namespace Dywham.Middleware.Endpoints.WebApi.Contracts
{
    public class PermissonExecutionResult
    {
        public ExecutionPermissionStatus Status { get; set; }

        public HttpStatusCode HttpStatusCode { get; set; }
    }
}