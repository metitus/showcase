namespace Xoolit.Microservices.Storage.Repositories
{
    public class XoolitVersionedEntity : XoolitEntity
    {
        public int Version { get; set; }
    }
}