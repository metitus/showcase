﻿using System;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using Dywham.Middleware.Storage.Repositories;
using Dywham.Middleware.Storage.Repositories.Contracts;
using Xoolit.Microservices.Endpoints.WebApi.Providers.Security.Repositories.Entities;

namespace Xoolit.Microservices.Endpoints.WebApi.Providers.Security.Repositories
{
    public class ResourceRepository : Repository<ResourceEntity, SecurityDatabaseContext>, IResourceRepository
    {
        public ResourceRepository(IRepositoriesSettings setting, IDatabaseContextFactory<SecurityDatabaseContext> databaseContextFactory) : base(setting, databaseContextFactory)
        { }


        public async Task<ResourceEntity> GetAsync(string pattern, CancellationToken token)
        {
            return await DbContext.Resources.AsNoTracking()
                .FirstOrDefaultAsync(x => x.Pattern.Equals(pattern, StringComparison.OrdinalIgnoreCase), token);
        }
    }
}
