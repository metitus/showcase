﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Dywham.Middleware.Storage.Repositories.Contracts;
using Xoolit.Microservices.Accounts.Repositories.Entities;

namespace Xoolit.Microservices.Accounts.Repositories
{
    public interface IAccountRepository : IRepository<AccountEntity>
    {
        Task<AccountEntity> GetAsync(Guid id, CancellationToken token);

        Task<AccountEntity> GetByUsernameOrEmailAsync(string username, string email, CancellationToken token);

        Task<AccountEntity> GetByActivationAsync(string activationCode, CancellationToken token);

        Task AddAsync(AccountEntity account, CancellationToken token);

        Task UpdateAsync(AccountEntity account, CancellationToken token);
    }
}
