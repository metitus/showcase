﻿using System;

namespace Xoolit.Microservices.Endpoint.Bus.Contracts.Messages
{
    public interface IContainsUserSessionData
    {
        Guid SessionId { get; set; }

        Guid UserId { get; set; }
    }
}