﻿using System;

namespace Dywham.Middleware.Endpoints.Bus.Contracts.Messages.Events
{
    public class MiddlewareEvent : IMiddlewareMessage
    {
        public Guid MessageId { get; set; }

        public DateTime DateTime { get; set; }

        public Guid RequestedByTrackerId { get; set; }

        public Guid GeneratedInTheContextOf { get; set; }
    }
}