﻿namespace Dywham.Middleware.Endpoints
{
    public class DywhamEndpointConfiguration
    {
        public EndpointExecutionMode ExecutionMode { get; set; }

        public string EndpointName { get; set; }
    }
}