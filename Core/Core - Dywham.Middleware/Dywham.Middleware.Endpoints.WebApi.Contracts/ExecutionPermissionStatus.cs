﻿namespace Dywham.Middleware.Endpoints.WebApi.Contracts
{
    public enum ExecutionPermissionStatus
    {
        Forbidden = 0,
        NotAuthenticated = 1,
        Allowed = 2,
        Unknown = 3
    }
}