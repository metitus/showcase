using NServiceBus;

namespace Dywham.Middleware.Endpoints.Bus
{
    public class MessageSessionContainer
    {
        public MessageSessionContainer(IMessageSession originalValue)
        {
            Value = originalValue;
        }


        public IMessageSession Value { get; set; }
    }
}