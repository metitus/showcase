﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using Common.Logging;
using Dywham.Middleware.Endpoints.WebApi.Contracts;
using Xoolit.Microservices.Endpoints.WebApi.Contracts;
using Xoolit.Microservices.Endpoints.WebApi.Contracts.Commands;
using Xoolit.Microservices.Endpoints.WebApi.Contracts.Security;
using Xoolit.Microservices.Endpoints.WebApi.Providers.Security.Repositories;
using Xoolit.Microservices.Endpoints.WebApi.Providers.Security.Repositories.Entities;

namespace Xoolit.Microservices.Endpoints.WebApi.Providers.Security
{
    public class XoolitSecurityProvider : IXoolitSecurityProvider
    {
        private readonly ILog _log = LogManager.GetLogger("XoolitSecurityProvider");
        private const string SessionHeaderKey = "Dywham-Session-Id";
        private static readonly PermissonExecutionResult DefaultResponse = new PermissonExecutionResult
        {
            HttpStatusCode = HttpStatusCode.Unauthorized,
            Status = ExecutionPermissionStatus.NotAuthenticated
        };


        public ISessionRepository SessionRepository { get; set; }

        public IResourceRepository ResourceRepository { get; set; }

        public XoolitApiEndpointConfiguration XoolitApiEndpointConfiguration { get; set; }


        public async Task<PermissonExecutionResult> ValidateResourcePermissionAsync(HttpActionContext actionContext, string resource,
            HttpRequestMessage httpRequestMessage, CancellationToken token)
        {
            return await ValidateSessionAsync(actionContext, httpRequestMessage, token);
        }

        public async Task<PermissonExecutionResult> ValidateSessionAsync(HttpActionContext actionContext, HttpRequestMessage httpRequestMessage, CancellationToken token)
        {
            try
            {
                SessionEntity sessionEntity = null;

                if (httpRequestMessage.Headers.Contains(SessionHeaderKey))
                {
                    Guid sessionId;

                    if (!Guid.TryParse(httpRequestMessage.Headers
                        .First(x => x.Key.Equals(SessionHeaderKey, StringComparison.OrdinalIgnoreCase)).Value.First(), out sessionId))
                    {
                        return DefaultResponse;
                    }

                    sessionEntity = await SessionRepository.GetAsync(sessionId, token);

                    if (sessionEntity != null)
                    {
                        foreach (var actionArgument in actionContext.ActionArguments.Where(x => x.Value is IRequiresIdentity))
                        {
                            ((IRequiresIdentity)actionArgument.Value).UserId = sessionEntity.UserId;
                        }

                        var endpoint = actionContext.ControllerContext.Controller as XoolitApiEndpoint;

                        if (endpoint != null)
                        {
                            endpoint.CurrentSessionInfo = new CurrentSessionInfo
                            {
                                AccountId = sessionEntity.UserId,
                                SessionId = sessionEntity.ExternalId
                            };
                        }
                    }
                }

                var secureRouteAttributes = actionContext.ActionDescriptor.GetCustomAttributes<SecureRouteAttribute>();

                if (!secureRouteAttributes.Any() || secureRouteAttributes.First().ResourceAccessMode == ResourceAccessMode.Anonymous)
                {
                    return new PermissonExecutionResult
                    {
                        Status = ExecutionPermissionStatus.Allowed
                    };
                }

                if (sessionEntity == null)
                {
                    return new PermissonExecutionResult
                    {
                        HttpStatusCode = HttpStatusCode.Forbidden,
                        Status = ExecutionPermissionStatus.Forbidden
                    };
                }

                if (sessionEntity.DateTimeFinished.HasValue)
                {
                    return new PermissonExecutionResult
                    {
                        HttpStatusCode = HttpStatusCode.Forbidden,
                        Status = ExecutionPermissionStatus.Forbidden
                    };
                }

                return new PermissonExecutionResult
                {
                    HttpStatusCode = HttpStatusCode.Accepted,
                    Status = ExecutionPermissionStatus.Allowed
                };
            }
            catch (Exception ex)
            {
                _log.Error(ex);

                return DefaultResponse;
            }
        }
    }
}