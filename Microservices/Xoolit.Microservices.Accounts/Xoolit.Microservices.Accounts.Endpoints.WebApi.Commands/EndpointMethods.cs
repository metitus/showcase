﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using AutoMapper;
using Dywham.Middleware.Endpoints.WebApi.Commands;
using NServiceBus;
using Swashbuckle.Swagger.Annotations;
using Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Commands;
using Xoolit.Microservices.Accounts.Endpoints.WebApi.Commands.Contracts;
using Xoolit.Microservices.Endpoints.WebApi.Contracts.Security;

namespace Xoolit.Microservices.Accounts.Endpoints.WebApi.Commands
{
    [EnableCors("*", "*", "*")]
    public class EndpointMethods : ApiCommandsEndpoint
    {
        public EndpointSettings EndpointSettings { get; set; }


        [Route("Accounts"), SecureRoute(ResourceAccessMode.ActiveSession)]
        [SwaggerOperation(Tags = new[] { "Accounts" })]
        [SwaggerResponse(HttpStatusCode.Accepted), SwaggerResponseRemoveDefaults]
        public async Task CreateAccount([FromBody]CreateAccountCommandModel model)
        {
            await Bus.Send(Mapper.Map<CreateAccountCommand>(model));
        }

        /// <param name="model">Activation details</param>
        /// <remarks>Activates the account associated with the given activation code</remarks>
        /// <returns></returns>
        [HttpPost]
        [Route("Accounts/activate")]
        [SwaggerOperation(Tags = new[] { "Accounts" })]
        [SwaggerResponse(HttpStatusCode.Accepted), SwaggerResponseRemoveDefaults]
        public async Task ActivateAccount([FromBody]ActivateAccountCommandModel model)
        {
            await Bus.Send(Mapper.Map<ActivateAccountCommand>(model));
        }

        [HttpPost]
        [Route("Accounts/sessions")]
        [SwaggerOperation(Tags = new[] { "Accounts" })]
        [SwaggerResponse(HttpStatusCode.Accepted), SwaggerResponseRemoveDefaults]
        public async Task CreateSession([FromBody]StartAccountSessionWithPasswordCommandModel model)
        {
            var command = Mapper.Map<StartAccountSessionWithPasswordCommand>(model);

            command.IpAddress = IpAdress;

            await Bus.Send(command);
        }
    }
}