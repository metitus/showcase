﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Dywham.Middleware.Endpoints.Bus.Contracts;
using NServiceBus;
using Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts;
using Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Commands;
using Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Events;
using Xoolit.Microservices.Accounts.Providers.PasswordHashing;
using Xoolit.Microservices.Accounts.Providers.TokenGenerator;
using Xoolit.Microservices.Accounts.Repositories;
using Xoolit.Microservices.Accounts.Repositories.Entities;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Handlers
{
    public class CreateAccountCommandHandler : IHandleMessages<CreateAccountCommand>
    {
        public IPasswordHashingProvider PasswordHashingProvider { get; set; }

        public IDywhamHandlingExecution DywhamHandlingExecution { get; set; }

        public IAccountRepository AccountRepository { get; set; }

        public ITokenGenerator TokenGenerator { get; set; }

        public EndpointSettings EndpointSettings { get; set; }


        public async Task Handle(CreateAccountCommand message, IMessageHandlerContext context)
        {
            await DywhamHandlingExecution.HandlesAsync(context, message, async executionContext =>
            {
                var accountEntity = new AccountEntity
                {
                    FirstName = message.FirstName,
                    Surname = message.Surname,
                    Email = message.Email,
                    Mobile = message.Mobile,
                    Username = string.IsNullOrWhiteSpace(message.Username)
                        ? message.Email
                        : message.Username,
                    DateTimeCreated = executionContext.ExecutionStartDateTime,
                    DateTimeLastChanged = executionContext.ExecutionStartDateTime,
                    ExternalId = Guid.NewGuid(),
                    EmailActivationCode = TokenGenerator.GenerateEmailToken(),
                    SmsActivationCode = TokenGenerator.GenerateSmsToken(),
                    PasswordHash = PasswordHashingProvider.CreateHash(message.Password),
                    Version = 1,
                    AccountStateId = (int) (string.IsNullOrWhiteSpace(message.Mobile)
                        ? AccountState.AwaitingEmailActivation
                        : AccountState.AwaitingEmailAndSmsVerification)
                };

                await AccountRepository.AddAsync(accountEntity, executionContext.CancellationToken);

                var accountCreatedEvent = Mapper.Map<AccountCreatedEvent>(message);

                accountCreatedEvent.Id = accountEntity.ExternalId;
                accountCreatedEvent.Version = 0;
                accountCreatedEvent.Username = accountEntity.Username;
                accountCreatedEvent.AccountStateId = accountEntity.AccountStateId;
                accountCreatedEvent.EmailActivationCode = accountEntity.EmailActivationCode;
                accountCreatedEvent.SmsActivationCode = accountEntity.SmsActivationCode;
                accountCreatedEvent.Rating = EndpointSettings.InitialAccountRating;

                await executionContext.DywhamBus.PublishAsync(accountCreatedEvent,
                    executionContext.CancellationToken);
            });
        }
    }
}