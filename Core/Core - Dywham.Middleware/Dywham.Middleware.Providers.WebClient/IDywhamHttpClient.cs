﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Dywham.Middleware.Providers.WebClient
{
    public interface IDywhamHttpClient
    {
        DywhamHttpClient AddDefaultRequestHeaders(Dictionary<string, object> defaultRequestHeaders);

        void DoDelete(SubmitDywhamHttpRequestParams httpRequestParams = null);

        Task DoDeleteAsync(SubmitDywhamHttpRequestParams httpRequestParams, CancellationToken ct);

        Task DoDeleteAsync(string url, CancellationToken ct);

        Task DoDeleteAsync(string url, object payload, CancellationToken ct);

        T DoGet<T>(DywhamHttpRequestParams submitSettings);

        T DoGet<T>(DywhamHttpRequestParams submitSettings, Dictionary<string, object> requestHeaders);

        Task<T> DoGetAsync<T>(string url, CancellationToken ct);

        Task<T> DoGetAsync<T>(DywhamHttpRequestParams submitSettings, CancellationToken ct);

        Task<T> DoGetAsync<T>(Dictionary<string, object> requestHeaders, CancellationToken ct);

        void DoPatch(SubmitDywhamHttpRequestParams httpRequestParams = null);

        Task DoPatchAsync(SubmitDywhamHttpRequestParams httpRequestParams, CancellationToken ct);

        Task DoPatchAsync(string url, object payload, CancellationToken ct);

        void DoPost(SubmitDywhamHttpRequestParams httpRequestParams = null);

        Task DoPostAsync(SubmitDywhamHttpRequestParams httpRequestParams, CancellationToken ct);

        Task DoPostAsync(string url, CancellationToken ct);

        Task DoPostAsync(string url, object payload, CancellationToken ct);

        void DoPut(SubmitDywhamHttpRequestParams httpRequestParams = null);

        Task DoPutAsync(SubmitDywhamHttpRequestParams httpRequestParams, CancellationToken ct);

        Task DoPutAsync(string url, CancellationToken ct);

        Task DoPutAsync(string url, object payload, CancellationToken ct);

        Task DoRequestAsync(HttpMethod httpMethod, DywhamHttpRequestParams httpRequestParams, CancellationToken ct);

        Task<T> DoRequestAsync<T>(HttpMethod httpMethod, string url, CancellationToken ct);

        Task<T> DoRequestAsync<T>(HttpMethod httpMethod, DywhamHttpRequestParams httpRequestParams, CancellationToken ct);

        Task<T> DoRequestAsync<T>(HttpMethod httpMethod, string url, object payload, CancellationToken ct);
    }
}