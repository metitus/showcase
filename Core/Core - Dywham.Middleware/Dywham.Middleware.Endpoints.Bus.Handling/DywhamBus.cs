﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Dywham.Middleware.Endpoints.Bus.Contracts;
using Dywham.Middleware.Endpoints.Bus.Contracts.Messages;
using Dywham.Middleware.Endpoints.Bus.Contracts.Messages.Commands;
using Dywham.Middleware.Endpoints.Bus.Contracts.Messages.Events;
using Dywham.Middleware.Providers.EventStore.Contracts;
using NServiceBus;

namespace Dywham.Middleware.Endpoints.Bus.Handling
{
    public class DywhamBus : IDywhamBus
    {
        public DywhamBus(IMessageHandlerContext messageHandlerContext, IEventStoreProvider eventStoreProvider, IMiddlewareMessage originalMessage)
        {
            MessageHandlerContext = messageHandlerContext;

            OriginalMessage = originalMessage;

            EventStoreProvider = eventStoreProvider;
        }


        private IMessageHandlerContext MessageHandlerContext { get; }

        private IMiddlewareMessage OriginalMessage { get; }

        private IEventStoreProvider EventStoreProvider { get; }


        public async Task SendAsync(MiddlewareCommand message, CancellationToken token)
        {
            SetOutgoingDefaultHeaders(message);

            await MessageHandlerContext.Send(message);
        }

        public async Task SendLocalAsync(MiddlewareCommand message, CancellationToken token)
        {
            SetOutgoingDefaultHeaders(message);

            await MessageHandlerContext.SendLocal(message);
        }

        public async Task<MiddlewareEvent> PublishAsync(MiddlewareEvent message, CancellationToken token, bool storeEvent = true)
        {
            return await PublishAsync(message, null, token, storeEvent);
        }

        public async Task<MiddlewareEvent> PublishAsync(MiddlewareEvent message, MiddlewareCommand command, CancellationToken token, bool storeEvent = true)
        {
            SetOutgoingDefaultHeaders(message);

            await MessageHandlerContext.Publish(message);

            // ReSharper disable once SuspiciousTypeConversion.Global
            var eventIdentity = message as IEventStoreIdentity;

            if (!storeEvent || eventIdentity == null || EventStoreProvider == null) return message;

            await EventStoreProvider?.StoreAsync(new Event
            {
                DateTime = message.DateTime,
                OriginatateInTheContextOfIdentifier = command?.MessageId,
                Payload = message,
                OriginatateInTheContextOfPayload = command,
                Identity = eventIdentity.Id,
                Version = eventIdentity.Version
            }, token);

            return message;
        }

        private void SetOutgoingDefaultHeaders(IMiddlewareMessage message)
        {
            message.DateTime = DateTime.UtcNow;

            if (message.RequestedByTrackerId == Guid.Empty)
            {
                message.RequestedByTrackerId = OriginalMessage.RequestedByTrackerId;
            }

            message.GeneratedInTheContextOf = OriginalMessage.MessageId;
        }
    }
}