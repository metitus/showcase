﻿using System.Web;
using System.Web.Http;
using Dywham.Middleware.Endpoints.WebApi.Filters;

namespace Dywham.Middleware.Endpoints.WebApi
{
    [ApiRequestExecutionFilter]
    public class ApiEndpoint : ApiController
    {
        private const string HttpContextKey = "MS_HttpContext";
        private const string RemoteEndpointMessage = "System.ServiceModel.Channels.RemoteEndpointMessageProperty";


        public ApiEndpoint()
        {
            if (HttpContext.Current != null)
            {
                IpAdress = HttpContext.Current.Request.UserHostAddress;
            }
            else
            {
                if (Request.Properties.ContainsKey(HttpContextKey))
                {
                    dynamic ctx = Request.Properties[HttpContextKey];

                    if (ctx != null)
                    {
                        IpAdress = ctx.Request.UserHostAddress;
                    }

                    return;
                }

                dynamic remoteEndpoint = Request.Properties[RemoteEndpointMessage];

                IpAdress = remoteEndpoint.Address;
            }
        }


        public string IpAdress { get; }
    }
}