﻿USE [Xoolit.Microservices.Accounts]
GO
INSERT [dbo].[SessionFinishedTypes] ([Id], [Name]) VALUES (1, N'Timedout')
GO
INSERT [dbo].[SessionFinishedTypes] ([Id], [Name]) VALUES (2, N'UserLoggedOut')
GO