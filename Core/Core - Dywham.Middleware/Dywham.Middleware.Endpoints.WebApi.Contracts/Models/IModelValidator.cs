﻿using System.Net.Http;

namespace Dywham.Middleware.Endpoints.WebApi.Contracts.Models
{
    public interface IModelValidator
    { }

    public interface IModelValidator<in T> : IModelValidator where T: class, new()
    {
        ValidationResult Validate(T model, HttpRequestMessage httpRequestMessage);
    }
}