﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Xoolit.Microservices.Accounts.Repositories.Entities
{

    [Table("Sessions")]
    public class SessionEntity
    {
        public long Id { get; set; }

        public long AccountId { get; set; }

        public Guid ExternalId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DateTimeStarted { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? DateTimeFinished { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastDateTimePing { get; set; }

        [Required]
        [StringLength(50)]
        public string IpAddress { get; set; }

        public int? SessionFinishedTypeId { get; set; }

        public virtual AccountEntity Account { get; set; }

        public Guid UserId { get; set; }
    }
}
