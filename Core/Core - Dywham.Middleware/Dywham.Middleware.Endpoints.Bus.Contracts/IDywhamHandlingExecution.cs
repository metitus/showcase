﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Dywham.Middleware.Endpoints.Bus.Contracts.Messages;
using NServiceBus;

namespace Dywham.Middleware.Endpoints.Bus.Contracts
{
    public interface IDywhamHandlingExecution
    {
        Func<IMiddlewareMessage, CancellationToken, Task> BeforeHandlingAsync { get; }

        Func<IMiddlewareMessage, CancellationToken, Task> AfterHandlingAsync { get; }

        Func<IMiddlewareMessage, Exception, CancellationToken, Task> OnExceptionAsync { get; }

        IDywhamBus DywhamBus { get; set; }


        IDywhamHandlingExecution WithBeforeHandlingAsync(Func<IMiddlewareMessage, CancellationToken, Task> beforeHandlingAsync);

        IDywhamHandlingExecution WithAfterHandlingAsync(Func<IMiddlewareMessage, CancellationToken, Task> afterHandlingAsync);

        IDywhamHandlingExecution WithWithOnExceptionAsync(Func<IMiddlewareMessage, Exception, CancellationToken, Task> onExceptionAsync);


        Task HandlesAsync(IMessageHandlerContext messageHandlerContext, IMiddlewareMessage message,
            Func<IDywhamHandlingExecutionContext, Task> messageAction);

        Task HandlesAsync(IMessageHandlerContext messageHandlerContext, IMiddlewareMessage message,
            Func<IDywhamHandlingExecutionContext, Task> messageAction, CancellationToken? token);
    }
}