﻿namespace Dywham.Middleware.Endpoints.WebApi.Contracts
{
    public class ApiEndpointConfiguration : DywhamEndpointConfiguration
    {
        public bool CreateApiModelParametersIfNull { get; set; }
    }
}