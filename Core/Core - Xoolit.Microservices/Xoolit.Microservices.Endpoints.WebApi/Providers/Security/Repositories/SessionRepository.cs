using System;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using Dywham.Middleware.Storage.Repositories;
using Dywham.Middleware.Storage.Repositories.Contracts;
using Xoolit.Microservices.Endpoints.WebApi.Providers.Security.Repositories.Entities;

namespace Xoolit.Microservices.Endpoints.WebApi.Providers.Security.Repositories
{
    public class SessionRepository : Repository<SessionEntity, SecurityDatabaseContext>, ISessionRepository
    {
        public SessionRepository(IRepositoriesSettings setting, IDatabaseContextFactory<SecurityDatabaseContext> databaseContextFactory) : base(setting, databaseContextFactory)
        { }


        public async Task<SessionEntity> GetAsync(Guid sessionId, CancellationToken token)
        {
            return await DbContext.Sessions.AsNoTracking()
                .FirstOrDefaultAsync(x => x.ExternalId == sessionId , token);
        }
    }
}