﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dywham.Middleware.Storage.Repositories;
using Dywham.Middleware.Storage.Repositories.Contracts;
using Xoolit.Microservices.Accounts.Repositories.Entities;
using Z.EntityFramework.Plus;

namespace Xoolit.Microservices.Accounts.Repositories
{
    public class SessionRepository : Repository<SessionEntity, AccountsDatabaseContext>, ISessionRepository
    {
        public SessionRepository(IRepositoriesSettings setting, IDatabaseContextFactory<AccountsDatabaseContext> databaseContextFactory)
            : base(setting, databaseContextFactory)
        { }


        public async Task<SessionEntity> GetSingleAsync(Guid id, CancellationToken token)
        {
            return await DbContext.Sessions.AsNoTracking().FirstOrDefaultAsync(x => x.ExternalId == id, token);
        }

        public async Task<IList<SessionEntity>> GetByAsync(SessionEntityQueryParms query, CancellationToken token)
        {
            var expression = DbContext.Sessions.AsNoTracking().AsQueryable();

            if (!string.IsNullOrEmpty(query.IpAddress))
            {
                expression = expression.Where(x => x.IpAddress.Equals(query.IpAddress, StringComparison.OrdinalIgnoreCase));
            }

            if (query.DateTimeFinished.HasValue)
            {
                expression = expression.Where(x => x.DateTimeFinished == query.DateTimeFinished);
            }

            if (query.DateTimeStarted.HasValue)
            {
                expression = expression.Where(x => x.DateTimeStarted == query.DateTimeStarted);
            }

            if (query.LastDateTimePing.HasValue)
            {
                expression = expression.Where(x => x.LastDateTimePing == query.LastDateTimePing);
            }

            if (query.SessionFinishedTypeId.HasValue)
            {
                expression = expression.Where(x => x.SessionFinishedTypeId == query.SessionFinishedTypeId);
            }

            return await expression.ToListAsync(token);
        }

        public async Task AddAsync(SessionEntity session, CancellationToken token)
        {
            DbContext.Sessions.Add(session);

            await DbContext.SaveChangesAsync(token);
        }

        public async Task UpdateAsync(SessionEntity session, CancellationToken token)
        {
            DbContext.Entry(session).State = EntityState.Modified;

            await DbContext.SaveChangesAsync(token);
        }

        public async Task UpdateBulkAsync(Func<SessionEntity, bool> condition, SessionEntity newEntity, CancellationToken token)
        {
            await DbContext.Sessions.Where(entity => condition(entity))
                .UpdateAsync(entry => newEntity, token);
        }
    }
}