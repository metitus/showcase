﻿using Autofac;

namespace Xoolit.Microservices.Endpoints.WebApi.Providers.Security
{
    public class SecurityAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<XoolitSecurityProvider>().SingleInstance();

        }
    }
}