﻿using System;

namespace Xoolit.Microservices.Endpoints.WebApi.Contracts.Validations
{
    public class DateFieldModel : FieldModel
    {
        public static DateFieldModel Default = new DateFieldModel();

        public DateTime? Min { get; set; }

        public DateTime? Max { get; set; }

        public string Regex { get; set; }
    }
}