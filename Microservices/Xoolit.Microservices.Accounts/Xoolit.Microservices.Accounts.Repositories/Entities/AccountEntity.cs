﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Xoolit.Microservices.Storage.Repositories;

namespace Xoolit.Microservices.Accounts.Repositories.Entities
{
    [Table("Accounts")]
    public class AccountEntity : XoolitVersionedEntity
    {
        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public int AccountStateId { get; set; }

        public string EmailActivationCode { get; set; }

        public string PasswordHash { get; set; }

        public int? SmsActivationCode { get; set; }

        public virtual ICollection<SessionEntity> Sessions { get; set; }
    }
}