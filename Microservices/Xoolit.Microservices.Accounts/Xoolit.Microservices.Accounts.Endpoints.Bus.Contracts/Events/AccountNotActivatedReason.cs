﻿namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Events
{
    public enum AccountNotActivatedReason
    {
        EmailActivationCodeNotValid = 1,
        SmsActivationCodeNotValid = 2,
        AccountActive = 3,
        AccountDeleted = 4,
        AccountSuspended = 5
    }
}