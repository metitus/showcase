﻿using System.Net.Http.Headers;

namespace Dywham.Middleware.Endpoints.WebApi.Contracts.Providers
{
    public interface IApiRequestMutatorProvider
    {
        bool MutateRequest(ApiMessageRequestEnvelope commandEnvelope, object platformMessage);

        void MutateRequest(string resource, HttpHeaders httpHeaders);
    }
}
