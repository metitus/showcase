﻿using Dywham.Middleware.Endpoints.WebApi.Commands;
using Xoolit.Microservices.Endpoints.WebApi.Providers.Security;

namespace Xoolit.Microservices.Endpoints.WebApi.Commands
{
    public class XoolitApiCommandsEndpoint : ApiCommandsEndpoint
    {
        public CurrentSessionInfo CurrentSessionInfo { get; internal set; }
    }
}