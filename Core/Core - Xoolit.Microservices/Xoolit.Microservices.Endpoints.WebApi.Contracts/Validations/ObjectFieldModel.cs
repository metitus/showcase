namespace Xoolit.Microservices.Endpoints.WebApi.Contracts.Validations
{
    public class ObjectFieldModel : FieldModel
    {
        public static ObjectFieldModel Default = new ObjectFieldModel();

        public int? Min { get; set; }

        public int? Max { get; set; }
    }
}