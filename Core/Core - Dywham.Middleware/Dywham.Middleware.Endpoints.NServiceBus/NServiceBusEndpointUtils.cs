﻿using System;
using NServiceBus;

namespace Dywham.Middleware.Endpoints.NServiceBus
{
    public static class NServiceBusEndpointUtils
    {
        public static void ConfigureMessagesConventions(EndpointConfiguration configuration)
        {
            Func<Type, string, bool> checkTypeMatches = (type, endsWith) =>
                type?.Namespace != null && type.Name.EndsWith(endsWith);

            configuration.Conventions()
                .DefiningCommandsAs(type =>  checkTypeMatches(type, "Command"))
                .DefiningEventsAs(type => checkTypeMatches(type, "Event"));
        }
    }
}