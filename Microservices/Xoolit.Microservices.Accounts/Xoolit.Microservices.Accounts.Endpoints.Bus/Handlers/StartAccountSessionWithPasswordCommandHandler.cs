﻿using System;
using System.Threading.Tasks;
using Dywham.Middleware.Endpoints.Bus.Contracts;
using NServiceBus;
using Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Commands;
using Xoolit.Microservices.Accounts.Endpoints.Bus.Contracts.Events;
using Xoolit.Microservices.Accounts.Providers.PasswordHashing;
using Xoolit.Microservices.Accounts.Repositories;
using Xoolit.Microservices.Accounts.Repositories.Entities;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus.Handlers
{
    public class StartAccountSessionWithPasswordCommandHandler : IHandleMessages<StartAccountSessionWithPasswordCommand>
    {
        public IDywhamHandlingExecution DywhamHandlingExecution { get; set; }

        public IAccountRepository AccountRepository { get; set; }

        public ISessionRepository SessionRepository { get; set; }

        public IPasswordHashingProvider PasswordHashingProvider { get; set; }


        public async Task Handle(StartAccountSessionWithPasswordCommand message, IMessageHandlerContext messageHandlerContext)
        {
            await DywhamHandlingExecution.HandlesAsync(messageHandlerContext, message, async executionContext =>
            {
                var account = await AccountRepository.GetByUsernameOrEmailAsync(message.Username, message.Email,
                    executionContext.CancellationToken);

                if (account == null)
                {
                    await executionContext.DywhamBus.PublishAsync(new AccountOrPasswordInfoNotValidEvent(), executionContext.CancellationToken);

                    executionContext.Log.Info($"No account was found matching email: {message.Email} or username: {message.Username}");

                    return;
                }

                var session = new SessionEntity
                {
                    AccountId = account.Id,
                    ExternalId = Guid.NewGuid(),
                    DateTimeStarted = executionContext.ExecutionStartDateTime,
                    IpAddress = message.IpAddress,
                    LastDateTimePing = executionContext.ExecutionStartDateTime,
                    UserId = account.ExternalId                    
                };

                if (!PasswordHashingProvider.Verify(message.Password, account.PasswordHash))
                {
                    await executionContext.DywhamBus.PublishAsync(new AccountOrPasswordInfoNotValidEvent(), executionContext.CancellationToken);

                    executionContext.Log.Info($"Password hash for account: {account.ExternalId} did not match");

                    return;
                }

                await SessionRepository.AddAsync(session, executionContext.CancellationToken);

                await executionContext.DywhamBus.PublishAsync(new AccountSessionCreatedEvent
                {
                    AccountId = account.ExternalId,
                    IpAddress = message.IpAddress,
                    SessionId = session.ExternalId,
                    FirstName = account.FirstName,
                    Surname = account.Surname
                }, executionContext.CancellationToken);
            });
        }
    }
}