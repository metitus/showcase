﻿using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using Dywham.Middleware.Endpoints.WebApi.Commands.Contracts.Models;
using Dywham.Middleware.Endpoints.WebApi.Contracts.Models;
using ValidationResult = Dywham.Middleware.Endpoints.WebApi.Contracts.Models.ValidationResult;

namespace Xoolit.Microservices.Accounts.Endpoints.WebApi.Commands.Contracts
{
    public class CreateAccountCommandModel : ApiCommandModel, IModelValidator<CreateAccountCommandModel>
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string Surname { get; set; }

        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public string Mobile { get; set; }

        [Required]
        public string Culture { get; set; }


        public ValidationResult Validate(CreateAccountCommandModel model, HttpRequestMessage httpRequestMessage)
        {
            const int firstNameMin = 3;
            const int firstNameMax = 10;
            const int surmameMin = 3;
            const int surnameMax = 10;
            const int usernameMin = 5;
            const int usernameMax = 25;
            const int passwordMin = 5;
            const int passwordMax = 25;

            var validationResult = new ValidationResult();

            if (model.FirstName.Length < firstNameMin)
            {
                validationResult.ValidationIssues.Add("FirstName", $"Length cannot be lower than {firstNameMin}");
            }

            if (model.FirstName.Length > firstNameMax)
            {
                validationResult.ValidationIssues.Add("FirstName", $"Length cannot be higher than {firstNameMax}");
            }

            if (model.Surname.Length < surmameMin)
            {
                validationResult.ValidationIssues.Add("Surname", $"Length cannot be lower than {surmameMin}");
            }

            if (model.Surname.Length > surnameMax)
            {
                validationResult.ValidationIssues.Add("Surname", $"Length cannot be higher than {surnameMax}");
            }

            if (model.Username.Length < usernameMin)
            {
                validationResult.ValidationIssues.Add("Username", $"Length cannot be lower than {usernameMin}");
            }

            if (model.Username.Length > usernameMax)
            {
                validationResult.ValidationIssues.Add("Username", $"Length cannot be higher than {usernameMax}");
            }

            if (model.Password.Length < passwordMin)
            {
                validationResult.ValidationIssues.Add("Password", $"Length cannot be lower than {passwordMin}");
            }

            if (model.Password.Length > passwordMax)
            {
                validationResult.ValidationIssues.Add("Password", $"Length cannot be higher than {passwordMax}");
            }

            return validationResult;
        }
    }
}
