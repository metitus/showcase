﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;
using Dywham.Middleware.Endpoints.Bus.Contracts;
using Dywham.Middleware.Endpoints.Bus.Contracts.Messages;
using Dywham.Middleware.Providers.EventStore.Contracts;
using Newtonsoft.Json;
using NServiceBus;

namespace Dywham.Middleware.Endpoints.Bus.Handling
{
    public class DywhamHandlingExecution : IDywhamHandlingExecution
    {
        public DywhamEndpointBusConfiguration DywhamEndpointBusConfiguration { get; set; }

        public IEventStoreProvider EventStoreProvider { get; set; }

        public IDywhamBus DywhamBus { get; set; }

        public Func<IMiddlewareMessage, CancellationToken, Task> BeforeHandlingAsync { get; private set; }

        public Func<IMiddlewareMessage, CancellationToken, Task> AfterHandlingAsync { get; private set; }

        public Func<IMiddlewareMessage, Exception, CancellationToken, Task> OnExceptionAsync { get; private set; }


        public IDywhamHandlingExecution WithBeforeHandlingAsync(Func<IMiddlewareMessage, CancellationToken, Task> beforeHandlingAsync)
        {
            BeforeHandlingAsync = beforeHandlingAsync;

            return this;
        }

        public IDywhamHandlingExecution WithAfterHandlingAsync(Func<IMiddlewareMessage, CancellationToken, Task> afterHandlingAsync)
        {
            AfterHandlingAsync = afterHandlingAsync;

            return this;
        }

        public IDywhamHandlingExecution WithWithOnExceptionAsync(Func<IMiddlewareMessage, Exception, CancellationToken, Task> onExceptionAsync)
        {
            OnExceptionAsync = onExceptionAsync;

            return this;
        }

        public async Task HandlesAsync(IMessageHandlerContext messageHandlerContext, IMiddlewareMessage message,
            Func<IDywhamHandlingExecutionContext, Task> messageAction)
        {
            await HandlesAsync(messageHandlerContext, message, messageAction, null);
        }

        public async Task HandlesAsync(IMessageHandlerContext messageHandlerContext, IMiddlewareMessage message,
            Func<IDywhamHandlingExecutionContext, Task> messageAction, CancellationToken? token)
        {
            var messageTypeName = message.GetType().Name;
            var cancellationToken = token ?? CancellationToken.None;
            var log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            log.Info($"Started handling - {messageTypeName}:{message.MessageId} with message content -> {JsonConvert.SerializeObject(message)}");

            try
            {
                BeforeHandlingAsync?.Invoke(message, cancellationToken);

                DywhamBus = new DywhamBus(messageHandlerContext, EventStoreProvider, message);

                await messageAction(new DywhamHandlingExecutionContext
                {
                    CancellationToken = cancellationToken,
                    DywhamBus = DywhamBus,
                    ExecutionStartDateTime = DateTime.UtcNow,
                    Log = log
                });

                AfterHandlingAsync?.Invoke(message, cancellationToken);

                log.Info($"Finished handling - {messageTypeName}:{message.MessageId}");
            }
            catch (Exception ex)
            {
                log.Error($"Handling message - {messageTypeName}:{message.MessageId} finished with the following exception", ex);

                OnExceptionAsync?.Invoke(message, ex, cancellationToken);
            }
        }
    }
}