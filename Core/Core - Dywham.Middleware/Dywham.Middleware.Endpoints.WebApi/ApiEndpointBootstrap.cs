﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.ExceptionHandling;
using Autofac;
using Autofac.Integration.WebApi;
using Dywham.Middleware.Endpoints.WebApi.Contracts.Providers;
using Dywham.Middleware.Endpoints.WebApi.Handlers;
using Dywham.Middleware.Utils;
using Newtonsoft.Json.Serialization;
using Owin;
using Common.Logging;
using Dywham.Middleware.Endpoints.WebApi.Contracts;
using Dywham.Middleware.Endpoints.WebApi.Contracts.Models;
using Swashbuckle.Application;

namespace Dywham.Middleware.Endpoints.WebApi
{
    public abstract class ApiEndpointBootstrap : EndpointBootstrap
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(EndpointUtils));


        protected EndpointExecutionMode ExecutionMode { get; set; } = EndpointExecutionMode.Release;

        protected bool EnableSwagger { get; set; } = true;

        protected string EndpointVersion { get; set; } = "EndpointVersion";

        protected string EndpointDescription { get; set; } = "EndpointDescription";

        protected string EndpointXmlCommentsFile { get; set; } = "";

        protected string ApiControllersSuffix { get; set; }

        protected bool CreateApiModelParametersIfNull { get; set; } = true;


        public void Configuration(IAppBuilder app)
        {
            try
            {
                Configuration(app, new HttpConfiguration());
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

                var exception = ex as ReflectionTypeLoadException;

                if (exception == null) throw;

                foreach (var loaderException in exception.LoaderExceptions)
                {
                    Logger.Error(loaderException);
                }

                throw;
            }
        }

        public virtual void Configuration(IAppBuilder app, HttpConfiguration config)
        {
            var uri = new UriBuilder(Assembly.GetExecutingAssembly().CodeBase);

            DefaultExecutionPath = Uri.UnescapeDataString(uri.Path);
            DefaultExecutionPath = Path.GetDirectoryName(DefaultExecutionPath);

            config.Services.Clear(typeof(IExceptionHandler));
            
            var suffix = typeof(DefaultHttpControllerSelector).GetField("ControllerSuffix", BindingFlags.Static | BindingFlags.Public);

            if (suffix != null) suffix.SetValue(null, string.Empty);

            ContainerBuilder = InitializeDiContainer();

            SetSettings();

            OnEndpointConfigurationStart(app, config, ContainerBuilder);

            var container = ContainerBuilder.Build();

            config.Services.Replace(typeof(IExceptionHandler), new GlobalExceptionHandler(OnGlobalUnhandledExceptionRaised));
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            config.MapHttpAttributeRoutes();
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;

            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
            app.UseWebApi(config);

            LoadMappings();

            if (EnableSwagger)
            {
                config.EnableSwagger(c =>
                    {
                        c.RootUrl(req => new Uri(req.RequestUri, req.GetRequestContext().VirtualPathRoot).ToString());
                        c.SingleApiVersion(EndpointVersion, EndpointName);
                        c.UseFullTypeNameInSchemaIds();
                        c.DescribeAllEnumsAsStrings();

                        if (!string.IsNullOrWhiteSpace(EndpointXmlCommentsFile))
                        {
                            c.IncludeXmlComments($@"{AppDomain.CurrentDomain.BaseDirectory}\{EndpointXmlCommentsFile}");
                        }
                    }).EnableSwaggerUi();
            }

            OnEndpointConfigurationCompleted(app, container);
        }

        private ContainerBuilder InitializeDiContainer()
        {
            var builder = new ContainerBuilder();
            var assemblies = AssemblyUtils.Assemblies(DefaultExecutionPath);

            builder.RegisterTypes(assemblies.GetAllTypesExceptSubtypesOf(typeof(ApiController)))
                .Where(t => typeof(IModelValidator).IsAssignableFrom(t)
                            || typeof(IApiRequestMutatorProvider).IsAssignableFrom(t)
                            || typeof(ISecurityProvider).IsAssignableFrom(t))
                .AsImplementedInterfaces()
                .InstancePerRequest()
                .PropertiesAutowired();

            builder.Register(ctx => new ApiEndpointConfiguration
            {
                ExecutionMode = ExecutionMode,
                CreateApiModelParametersIfNull = CreateApiModelParametersIfNull
            }).As<ApiEndpointConfiguration>()
            .SingleInstance()
            .PropertiesAutowired();

            builder.RegisterApiControllers(string.IsNullOrWhiteSpace(ApiControllersSuffix)
                    ? "EndpointMethods"
                    : ApiControllersSuffix, assemblies)
                .PropertiesAutowired();

            builder.RegisterAssemblyModules(assemblies);

            return builder;
        }

        protected virtual void OnEndpointConfigurationStart(IAppBuilder app, HttpConfiguration config, ContainerBuilder builder)
        { }

        protected virtual void OnEndpointConfigurationCompleted(IAppBuilder app, IContainer container)
        { }

        public virtual void OnGlobalUnhandledExceptionRaised(ExceptionHandlerContext context)
        { }
    }

    public abstract class ApiEndpointBootstrap<T> : ApiEndpointBootstrap where T : SettingsForApiEndpoint
    {
        public T EndpointSettings { get; set; }


        protected override void SetSettings()
        {
            var settings = EndpointUtils.LoadComponentsConfigurationFilesAndSet<T>(typeof(T),
                HttpRuntime.BinDirectory, this, ContainerBuilder);

            if(settings.ExecutionMode.HasValue)
            {
                ExecutionMode = settings.ExecutionMode.Value;
            }

            if (!string.IsNullOrWhiteSpace(settings.EndpointVersion))
            {
                EndpointVersion = settings.EndpointVersion;
            }

            if (!string.IsNullOrWhiteSpace(settings.EndpointDescription))
            {
                EndpointDescription = settings.EndpointDescription;
            }
        }
    }
}