﻿using System.Web.Http;
using Autofac;
using Dywham.Middleware.Endpoints.WebApi.Commands;
using Microsoft.Owin;
using NServiceBus;
using Owin;
using Xoolit.Microservices.Accounts.Endpoints.WebApi.Commands;

[assembly: OwinStartup(typeof(EndpointBootstrap), "Configuration")]
namespace Xoolit.Microservices.Accounts.Endpoints.WebApi.Commands
{
    public class EndpointBootstrap : ApiCommandsEndpointBootstrap<EndpointSettings>
    {
        protected override void OnEndpointConfigurationStart(IAppBuilder app, HttpConfiguration config, ContainerBuilder builder)
        {
            config.EnableCors();

            base.OnEndpointConfigurationStart(app, config, builder);
        }
    }
}