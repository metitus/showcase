using System.Threading;
using System.Threading.Tasks;
using Dywham.Middleware.Providers.EventStore.EF.Repositories.Entities;
using Dywham.Middleware.Storage.Repositories;
using Dywham.Middleware.Storage.Repositories.Contracts;

namespace Dywham.Middleware.Providers.EventStore.EF.Repositories
{
    public class EventRepository : Repository<EventEntity, EventStoreDatabaseContextContext>, IEventRepository
    {
        public EventRepository(IRepositoriesSettings setting, IDatabaseContextFactory<EventStoreDatabaseContextContext> databaseContextFactory) : base(setting, databaseContextFactory)
        { }


        public void Add(EventEntity entity)
        {
            DbContext.Events.Add(entity);
        }

        public async Task AddAsync(EventEntity entity, CancellationToken token)
        {
            DbContext.Events.Add(entity);

            await DbContext.SaveChangesAsync(token);
        }
    }
}