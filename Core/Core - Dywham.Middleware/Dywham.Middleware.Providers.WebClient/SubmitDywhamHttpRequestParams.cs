﻿using Newtonsoft.Json;

namespace Dywham.Middleware.Providers.WebClient
{
    public class SubmitDywhamHttpRequestParams : DywhamHttpRequestParams
    {
        public object Data { get; set; }

        public JsonSerializerSettings JsonSerializerSettingsOutgoing { get; set; } = new JsonSerializerSettings();
    }
}