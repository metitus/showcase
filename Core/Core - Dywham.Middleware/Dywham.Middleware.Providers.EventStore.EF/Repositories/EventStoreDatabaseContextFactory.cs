﻿using Dywham.Middleware.Storage.Repositories.Contracts;

namespace Dywham.Middleware.Providers.EventStore.EF.Repositories
{
    public class EventStoreDatabaseContextFactory: IDatabaseContextFactory<EventStoreDatabaseContextContext>
    {
        public EventStoreDatabaseContextContext CreateInstance(string dataSource)
        {
            return new EventStoreDatabaseContextContext(dataSource);
        }
    }
}
