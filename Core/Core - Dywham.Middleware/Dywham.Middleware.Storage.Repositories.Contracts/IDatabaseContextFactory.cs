﻿using System.Data.Entity;

namespace Dywham.Middleware.Storage.Repositories.Contracts
{
    public interface IDatabaseContextFactory
    { }

    public interface IDatabaseContextFactory<out T> : IDatabaseContextFactory where T : DbContext
    {
        T CreateInstance(string dataSource);
    }
}