﻿using Common.Logging;
using System;
using System.Web.Http.ExceptionHandling;

namespace Dywham.Middleware.Endpoints.WebApi.Handlers
{
    public class GlobalExceptionHandler : ExceptionHandler
    {
        private readonly ILog _logger = LogManager.GetLogger("GlobalExceptionHandler");
        private readonly Action<ExceptionHandlerContext> _actionContext;


        public GlobalExceptionHandler(Action<ExceptionHandlerContext> actionContext)
        {
            _actionContext = actionContext;
        }


        public override void Handle(ExceptionHandlerContext context)
        {
            _logger.Error(context.Exception);

            _actionContext?.Invoke(context);

            base.Handle(context);
        }
    }
}