namespace Xoolit.Microservices.Accounts.Providers.TokenGenerator
{
    public interface ITokenGenerator
    {
        string GenerateEmailToken();

        int GenerateSmsToken();
    }
}