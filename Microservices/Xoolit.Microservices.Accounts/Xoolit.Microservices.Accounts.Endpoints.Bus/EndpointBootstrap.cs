﻿using Autofac;
using NServiceBus;
using Xoolit.Microservices.Accounts.Providers.PasswordHashing;
using Xoolit.Microservices.Accounts.Providers.TokenGenerator;
using Xoolit.Microservices.Endpoint.Bus;

namespace Xoolit.Microservices.Accounts.Endpoints.Bus
{
    public class EndpointBootstrap : XoolitBusEndpointBootstrap<EndpointSettings>
    {
        protected override void OnEndpointConfigurationStart(EndpointConfiguration configuration, ContainerBuilder builder)
        {
            EnableJobScheduler = true;

            builder.RegisterType<DefaultTokenGenerator>().As<ITokenGenerator>();
            builder.RegisterType<PasswordHashingProvider>().As<IPasswordHashingProvider>();

            base.OnEndpointConfigurationStart(configuration, builder);
        }
    }
}