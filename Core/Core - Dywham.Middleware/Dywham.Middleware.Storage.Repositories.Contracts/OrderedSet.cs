﻿namespace Dywham.Middleware.Storage.Repositories.Contracts
{
    public class OrderedSet
    {
        public bool Asc { get; set; } = true;

        public string ColumnName { get; set; }
    }
}