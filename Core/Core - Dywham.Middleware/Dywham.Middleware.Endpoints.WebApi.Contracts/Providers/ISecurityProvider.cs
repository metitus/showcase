﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;

namespace Dywham.Middleware.Endpoints.WebApi.Contracts.Providers
{
    public interface ISecurityProvider
    {
        Task<PermissonExecutionResult> ValidateResourcePermissionAsync(HttpActionContext actionContext, string resource,
            HttpRequestMessage httpRequestMessage, CancellationToken token);
    }
}