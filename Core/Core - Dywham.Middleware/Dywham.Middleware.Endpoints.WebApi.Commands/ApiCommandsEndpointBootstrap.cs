﻿using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using Autofac;
using NServiceBus;
using NServiceBus.Features;
using NServiceBus.Logging;
using Owin;
using Dywham.Middleware.Endpoints.NServiceBus;
using Dywham.Middleware.Endpoints.WebApi.Commands.Contracts;

namespace Dywham.Middleware.Endpoints.WebApi.Commands
{
    public abstract class ApiCommandsEndpointBootstrap : ApiEndpointBootstrap
    {
        protected virtual string NServiceBusEndpointName { get; private set; }

        protected virtual string NServiceBusLoggingLocation { get; set; } = HostingEnvironment.MapPath("~/Logs/");

        protected virtual string TransportConnectionString { get; set; } =
            "cacheSendConnection=true;journal=false;deadLetter=true;useTransactionalQueues=true";

        private IEndpointInstance EndpointInstance { get; set; }


        protected override void OnEndpointConfigurationStart(IAppBuilder app, HttpConfiguration config, ContainerBuilder builder)
        {
            LogManager.Use<CommonLoggingFactory>();

            // ReSharper disable once PossibleNullReferenceException
            NServiceBusEndpointName = GetType().FullName.Replace(".Configuration", string.Empty);

            var endpointConfiguration = new EndpointConfiguration(NServiceBusEndpointName);
            var transport = endpointConfiguration.UseTransport<MsmqTransport>();

            if (!string.IsNullOrWhiteSpace(TransportConnectionString))
            {
                transport.ConnectionString(TransportConnectionString);
            }

            endpointConfiguration.UseSerialization<JsonSerializer>();
            endpointConfiguration.UsePersistence<InMemoryPersistence>();
            endpointConfiguration.DisableFeature<MessageDrivenSubscriptions>();
            endpointConfiguration.SendOnly();

            ConfigureMessagesConventions(endpointConfiguration);

            EndpointInstance = Endpoint.Start(endpointConfiguration).GetAwaiter().GetResult();

            builder.Register(x => EndpointInstance)
                .As<IEndpointInstance>()
                .SingleInstance();

            OnEndpointConfigurationStart(app, builder, endpointConfiguration);
        }

        protected override void OnEndpointConfigurationCompleted(IAppBuilder app, IContainer container)
        {
            OnEndpointConfigurationCompleted(app, container, EndpointInstance);
        }

        protected virtual void ConfigureMessagesConventions(EndpointConfiguration configuration)
        {
            NServiceBusEndpointUtils.ConfigureMessagesConventions(configuration);
        }

        protected virtual void OnEndpointConfigurationStart(IAppBuilder app, ContainerBuilder builder, EndpointConfiguration endpointConfiguration)
        { }

        protected virtual void OnEndpointConfigurationCompleted(IAppBuilder app, IContainer container, IEndpointInstance endpoint)
        { }
    }

    public abstract class ApiCommandsEndpointBootstrap<T> : ApiEndpointBootstrap where T : SettingsForApiCommandsEndpoint
    {
        protected virtual string NServiceBusEndpointName { get; private set; }

        protected virtual string NServiceBusLoggingLocation { get; set; } = HostingEnvironment.MapPath("~/Logs/");

        protected virtual string TransportConnectionString { get; set; } = "cacheSendConnection=true;journal=false;deadLetter=true;useTransactionalQueues=true";

        private IEndpointInstance EndpointInstance { get; set; }

        public T EndpointSettings { get; set; }


        protected override void OnEndpointConfigurationStart(IAppBuilder app, HttpConfiguration config, ContainerBuilder builder)
        {
            LogManager.Use<CommonLoggingFactory>();

            // ReSharper disable once PossibleNullReferenceException
            NServiceBusEndpointName = GetType().FullName.Replace(".Configuration", string.Empty);

            var endpointConfiguration = new EndpointConfiguration(NServiceBusEndpointName);
            var transport = endpointConfiguration.UseTransport<MsmqTransport>();

            if (!string.IsNullOrWhiteSpace(TransportConnectionString))
            {
                transport.ConnectionString(TransportConnectionString);
            }

            endpointConfiguration.UseSerialization<JsonSerializer>();
            endpointConfiguration.UsePersistence<InMemoryPersistence>();
            endpointConfiguration.DisableFeature<MessageDrivenSubscriptions>();
            endpointConfiguration.SendOnly();

            ConfigureMessagesConventions(endpointConfiguration);

            EndpointInstance = Endpoint.Start(endpointConfiguration).GetAwaiter().GetResult();

            builder.Register(x => EndpointInstance)
                .As<IEndpointInstance>()
                .SingleInstance();

            OnEndpointConfigurationStart(app, builder, endpointConfiguration);
        }

        protected override void OnEndpointConfigurationCompleted(IAppBuilder app, IContainer container)
        {
            OnEndpointConfigurationCompleted(app, container, EndpointInstance);
        }

        protected virtual void ConfigureMessagesConventions(EndpointConfiguration configuration)
        {
            NServiceBusEndpointUtils.ConfigureMessagesConventions(configuration);
        }

        protected virtual void OnEndpointConfigurationStart(IAppBuilder app, ContainerBuilder builder, EndpointConfiguration endpointConfiguration)
        { }

        protected virtual void OnEndpointConfigurationCompleted(IAppBuilder app, IContainer container, IEndpointInstance endpoint)
        { }

        protected override void SetSettings()
        {
            var settings = EndpointUtils.LoadComponentsConfigurationFilesAndSet<T>(typeof(T),
                HttpRuntime.BinDirectory, this, ContainerBuilder);

            if (settings.ExecutionMode.HasValue)
            {
                ExecutionMode = settings.ExecutionMode.Value;
            }

            if (!string.IsNullOrWhiteSpace(settings.EndpointVersion))
            {
                EndpointVersion = settings.EndpointVersion;
            }

            if (!string.IsNullOrWhiteSpace(settings.EndpointDescription))
            {
                EndpointDescription = settings.EndpointDescription;
            }
        }
    }
}