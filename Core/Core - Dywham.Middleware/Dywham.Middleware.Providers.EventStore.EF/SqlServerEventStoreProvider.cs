﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Dywham.Middleware.Providers.EventStore.Contracts;
using Dywham.Middleware.Providers.EventStore.EF.Repositories;
using Dywham.Middleware.Providers.EventStore.EF.Repositories.Entities;
using Newtonsoft.Json;

namespace Dywham.Middleware.Providers.EventStore.EF
{
    public class SqlServerEventStoreProvider : IEventStoreProvider
    {
        public IEventRepository EventRepository { get; set; }


        public async Task StoreAsync(Event data, CancellationToken token)
        {
            var payloadType = data.Payload.GetType();

            var entity = new EventEntity
            {
                DateTime = data.DateTime ?? DateTime.UtcNow,
                FullName = payloadType.FullName,
                Name = payloadType.Name,
                Identity = data.Identity,
                Payload = JsonConvert.SerializeObject(data.Payload),
                Version = data.Version
            };

            if (entity.OriginatateInTheContextOfPayload != null)
            {
                var originatateInTheContextOfPayloadType = data.OriginatateInTheContextOfPayload.GetType();

                entity.OriginatateInTheContextOfFullName = originatateInTheContextOfPayloadType.FullName;
                entity.OriginatateInTheContextOfName = originatateInTheContextOfPayloadType.Name;
                entity.OriginatateInTheContextOfPayload = JsonConvert.SerializeObject(data.OriginatateInTheContextOfPayload);
                entity.OriginatateInTheContextOfIdentifier = data.OriginatateInTheContextOfIdentifier;

            }

            await EventRepository.AddAsync(entity, token);
        }
    }
}