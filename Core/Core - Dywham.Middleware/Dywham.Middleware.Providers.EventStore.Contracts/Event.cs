﻿using System;

namespace Dywham.Middleware.Providers.EventStore.Contracts
{
    public class Event
    {
        public Guid Identity { get; set; }

        public object Payload { get; set; }

        public int Version { get; set; }

        public DateTime? DateTime { get; set; }

        public Guid? OriginatateInTheContextOfIdentifier { get; set; }

        public object OriginatateInTheContextOfPayload { get; set; }
    }
}