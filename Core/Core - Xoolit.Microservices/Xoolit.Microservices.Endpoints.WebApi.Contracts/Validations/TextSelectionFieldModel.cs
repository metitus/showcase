﻿using System.Collections.Generic;

namespace Xoolit.Microservices.Endpoints.WebApi.Contracts.Validations
{
    public class TextSelectionFieldModel : FieldModel
    {
        public IList<TextSelectionFieldOptionModel> Options { get; set; }
    }
}