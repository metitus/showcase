﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dywham.Middleware.Providers.EventStore.EF.Repositories.Entities
{
    [Table("Events")]
    public class EventEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string FullName { get; set; }

        public int Version { get; set; }

        public Guid Identity { get; set; }

        public string Name { get; set; }

        public string Payload { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DateTime { get; set; }

        public Guid? OriginatateInTheContextOfIdentifier { get; set; }

        public string OriginatateInTheContextOfName { get; set; }

        public string OriginatateInTheContextOfFullName { get; set; }

        public string OriginatateInTheContextOfPayload { get; set; }
    }
}
